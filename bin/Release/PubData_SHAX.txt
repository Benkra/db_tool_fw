Master (BS21_SV):

CommPathSlaveIdx=BLE
Time=27.01.2024 10:54:00
DeviceVariantId=134 -> (LockSV - Lock App SV - BS20)
FW=1.1.549
PHI=84DXNE dez=140448714 hex=0x085F13CA   ChipID: 0xAD04EB465F489247
SID=0
 LID=0
   ProgState=_50_HAS_APP
DeviceClass=110/00 ProductName=BS21_SV
   State=none
   DeviceType_ProgCounter=_00_UNKNOWN_
   Sequence=23 SE-FW=4.23
   EEPROM=11 PCB_Version='K'
   LockLastAuthErr=0
   StateRaw=0x0080: localKeyError
   ConfigRaw=110
   NextDevice=0
   LocalSlaveAddress=0
_DevData.dataset=0
_DevData.deviceClass=111
_DevData.variantID=135
_DevData.FWVersion=11.2.37
_DevData.DrvVersion=0
_DevData.isCredential=0
_DevData.EEPROM_size=0x1E800
_DevData.configuration=0
_DevData.last_VN_buffer_entries=0
_DevData.Lv_EA_Command_Buffer_8=0
_DevData.sequence_counter=0
_DevData.progCounter=20.049 1:20.000 2:20.000 3:20.000 rd:20.014 MAX:20.049
_DevData.last_accesslist_entries=0
_DevData.Lv_Profile_Change_Requests=0
_DevData.Lv_VN_downstream_data=0
_DevData.PHI_ext=0
_DevData.devType=1
_DevData.subDeviceClass=0
_DevData.PHIRange=1
_DevData.localLockCount=1
_DevData.deviceClass= Base:111 Orig:111
_DevData.appClass=    Base:110 Orig:110
_DevData.deviceIdentifier=0xAD04EB465F489247
_DevData.FW_version= SE:SE 4.23 SSC:1.45 Bootloader:3.6
_DevData.logicalInputStates=0x4000: SABOTAGE     (_DevData.logicalInputs=0x8000:HANDLE_I(11)  0x4000:SABOTAGE(9))
_DevData.SD_Version=0x900A500
_DevData.client_IP_Addresses=0x00 0x00 0x00
_DevData.installedAppFW=         2.6  2.37   0.0   0.0   0.0   0.0   0.0   0.0
_DevData_APP-Table-Idx(fromSE) [_0_] [_1_] [_2_] [_3_] [_4_] [_5_] [_6_] [_7_]
_DevData.installedApps=            0     1     0     0     0     0     0     0
_DevData.installedAppClass=      111   110     0     0     0     0     0     0
_DevData.installedAppClassBase=  111   110     0     0     0     0     0     0
_DevData.installedAppVariantID=  135   134     0     0     0     0     0     0
_DevData.FormatDbVersion=       7.25  7.25   0.0   0.0   0.0   0.0   0.0   0.0
