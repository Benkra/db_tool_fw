﻿using DB_Tool_FW.DataManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DB_Tool_FW.DB_and_Data
{
    public interface IDataService
    {
        event EventHandler OnProzessInstanceChanged;

        ProzessModel ProzessModel { get; set; }
        DataModel CurrentDataModel { get; set; }
        BindingList<DataModel> DevDatas { get; set; }
        //BindingList<string> SearchableItems { get; set; }
        bool ProzessSaved { get; set; }
        bool DevDataSaved { get; set; }
        BindingList<string> DevDataDesignations { get; }
        BindingList<string> CompariaonSymbols { get; set; }

        void CreateNewProzess();
        //void DeleteAll();
        //void DeleteCurrentDataModelInstance();
        Dictionary<string, object> GetDataFromDb(int prozessID, int numberator);

        void ReadPublicDataFromDevice(int prozessId);
        object GetValue(string designation, int prozessId, int numberator);
        void WriteDevDataToDb();
        void WriteProzessToDb();
        //void ChangeProzessModel(ProzessModel prozessModel);
        void SetDbPath(string path);
        string GetDbConnection();
        void NewDb(string dbPath);
        List<ProzessModel> FindProzesses(string searchValue);
        void ArrangeDesignations();
        List<DataModel> FindDevData(string comparisonSymbol, string designation, string searchValue);
    }
}