﻿using DB_Tool_FW.DataManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Interop;

namespace DB_Tool_FW.DB_and_Data
{
    public sealed class DataService : IDataService
    {
        public event EventHandler OnProzessInstanceChanged;

        private IDB_Manager dBmanager;
        private SVDevCom sVDevCom;

        private DataModel currentDataModel;
        private ProzessModel prozessModel;

        private BindingList<DataModel> devDatas;
        private BindingList<String> searchableItems;
        private BindingList<String> devDataDesignations;
        private BindingList<string> compariaonSymbols;


        private bool prozessSaved;
        private bool devDataSaved;

        //Properties
        public ProzessModel ProzessModel { get => prozessModel; set => prozessModel = value; }
        public DataModel CurrentDataModel { get => currentDataModel; set => currentDataModel = value; }
        public BindingList<DataModel> DevDatas { get => devDatas; set => devDatas = value; }
        //public BindingList<string> SearchableItems { get => searchableItems; set => searchableItems = value; }
        public bool ProzessSaved { get => prozessSaved; set => prozessSaved = value; }
        public bool DevDataSaved { get => devDataSaved; set => devDataSaved = value; }
        public BindingList<string> DevDataDesignations { get => devDataDesignations; private set => devDataDesignations = value; }
        public BindingList<string> CompariaonSymbols { get => compariaonSymbols; set => compariaonSymbols = value; }

        public int Property
        {
            get => default;
            set
            {
            }
        }



        //Constructor
        public DataService(IDB_Manager dBmanager, SVDevCom sVDevCom)
        {
            this.dBmanager = dBmanager;
            this.sVDevCom = sVDevCom;


            InitDataSources();

        }


        private void InitDataSources()
        {


            DevDatas = new BindingList<DataModel>();
            DevDataDesignations = new BindingList<string>();
            compariaonSymbols  = new BindingList<string>();

            InitComarisonSymbolsList();



        }

        private void InitComarisonSymbolsList()
        {
            CompariaonSymbols.Add("=");
            CompariaonSymbols.Add(">");
            CompariaonSymbols.Add("<");


        }



        #region Database


        /// <summary>
        /// Makes the DbManager to Create a new Database
        /// </summary>
        /// <param name="dbPath"></param>
        public void NewDb(string dbPath)
        {
            dBmanager.CreateDb(dbPath);
        }

        /// <summary>
        /// Sets the DB Path for the DbManager instance
        /// </summary>
        /// <param name="path"></param>
        public void SetDbPath(string path)
        {
            dBmanager.SetUpDbConnection(path);
        }

        /// <summary>
        /// Checks if there is a valide DB Connecton and returns the path if the connnecton is valide. Otherwiese an empty string 
        /// </summary>
        /// <returns></returns>
        public string GetDbConnection()
        {
            return dBmanager.CheckDbConnection();
        }


      

        /// <summary>
        /// Makes the dBmanager Instance to write the currentDataModel into the database, returns an error msg 
        /// if the prozess instqnce is null
        /// </summary>
        /// <returns></returns>
        public void WriteDevDataToDb()
        {
            foreach (var devData in devDatas)
            {
                foreach(var rawDataModel in devData.RawDataModels)
                {
                    CurrentDataModel.AddRawDataModel(rawDataModel);
                }
            }
            
            dBmanager.WriteDevData(CurrentDataModel);
            
        }

        /// <summary>
        /// Makes the DbManager to write the prozess date to DB
        /// </summary>
        public void WriteProzessToDb()
        {
            dBmanager.WriteProzessData(ProzessModel);

        }

        /// <summary>
        /// Clears the whole database!!!!
        /// </summary>
        //public void DeleteAll()
        //{
        //    dBmanager.DeleteAll();
        //}



        /// <summary>
        /// Makes the DbManager to read the Prozess with the prozessId
        /// </summary>
        /// <param name="prozessId"></param>
        /// <returns></returns>
        public ProzessModel ReadProzessFromDb(int prozessId)
        {
            return dBmanager.ReadProzessFromDb(prozessId);
        }

        /// <summary>
        /// Makes the dBmanager instanze to read DevData from Database and sets 
        /// the currentDataModel Instance (prozessID)
        /// </summary>
        /// <param name="prozessId"></param>
        /// <paramref name="table"/>
        private void ReadDevDataFromDb(int prozessId)
        {
            if (CurrentDataModel == null || (int)CurrentDataModel.ProzessId != prozessId)
            {
                CurrentDataModel = dBmanager.ReadDevData(prozessId);


            }

        }

        /// <summary>
        /// Retunrs a string conaining all the DevData designations.  Like... "CommPathSlaveIdx"; "_DevData.dataset"; ..
        /// </summary>
        public void ArrangeDesignations()
        {
            DevDataDesignations = dBmanager.ReadDataDesignations();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prozessID"></param>
        /// <param name="numberator"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetDataFromDb(int prozessID, int numberator)
        {
            ReadDevDataFromDb(prozessID);

            return CurrentDataModel.GetDesignationValues(numberator);
        }

        /// <summary>
        /// Returns a List wiht all Prozesses that contain the searchValue
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public List<ProzessModel> FindProzesses(string searchValue)
        {
            List<ProzessModel> prozessModels = new List<ProzessModel>();
            List<int> prozessIds = new List<int>();

            prozessIds = dBmanager.FindProzess(searchValue);

            foreach (var prozessId in prozessIds)
            {
                prozessModels.Add(dBmanager.ReadProzessFromDb(prozessId));
            }


            return prozessModels;

        }

        /// <summary>
        /// Returns a List with all the DevData that contain the value
        /// </summary>
        /// <param name="comparisonSymbol"></param>
        /// <param name="designation"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public List<DataModel> FindDevData(string comparisonSymbol, string designation, string searchValue)
        {
            List<DataModel> dataModels = new List<DataModel>();
            List<int> prozessIds;

            prozessIds = dBmanager.FindDevData(comparisonSymbol, designation, searchValue);

            foreach (var prozessId in prozessIds)
            {
                dataModels.Add( dBmanager.ReadDevData(prozessId));
            }


            return dataModels;
        }

        #endregion




        #region DataModel handling


        /// <summary>
        /// Calls GetDataFromDb and returns the value of designation
        /// </summary>
        /// <param name="designation"></param>
        /// <param name="prozessId"></param>
        /// <param name="numberator"></param>
        /// <returns></returns>
        public object GetValue(string designation, int prozessId, int numberator)
        {
            var dictionary = new Dictionary<string, object>();

            object value;

            if (CurrentDataModel == null)
            {
                value = "nichts";
            }

            dictionary = GetDataFromDb(prozessId, numberator);

            value = dictionary[designation];




            return value;

        }

        //public void DeleteCurrentDataModelInstance()
        //{
        //    CurrentDataModel = null;
        //}

        #endregion





        #region ProzessModel handling

        /// <summary>
        /// Creates a new Prozess with the ProzessId one higher than the last in the DB
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void CreateNewProzess()
        {

            int prozessId = dBmanager.ReadLastProzessId();

            if (prozessId < 0)
            {
                throw new Exception("Bei der Erstellung des neuen Prozesses ist ein Fehler aufegreten! Details: intPorozessId konnte nicht erstellt werden ");
            }
            ProzessModel = new ProzessModel(prozessId + 1);

            devDatas.Clear();
            
        }


        #endregion




        #region Device handling

        /// <summary>
        /// Triggers a ReadPublid and saves the Data to the feeld currentDataModel
        /// </summary>
        /// <param name="prozessId"></param>
        /// <param name="numberator"></param>
        public void ReadPublicDataFromDevice(int prozessId)
        {

            //DataModel dataModel = null;

            try
            {
                sVDevCom = new SVDevCom();
                CurrentDataModel = sVDevCom.ReadPublicData(prozessId, devDatas.Count+1);
               

            }
            catch (Exception e)
            {
                sVDevCom = null;
            }
            DevDatas.Add(CurrentDataModel);
  
        }

        #endregion

        public void Method()
        {
            throw new System.NotImplementedException();
        }
    }
}
