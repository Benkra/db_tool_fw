﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Tool_FW.DB_and_Data
{
    public static class Enums
    {
        public enum Tables
        {
            Vorgang,
            PublicData,
            Attributes      
        }

        public enum DB_Manager
        {
            OK,
            Error
        }

        public enum DataServisMsgs
        {
            OK,
            Prozesss_not_saved,
            DevData_not_saved,
            No_prozess_instance

        }


    }
}
