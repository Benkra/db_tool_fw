﻿using DB_Tool_FW.DataManagement;
using DB_Tool_FW.DB_and_Data;
using System.Collections.Generic;
using System.ComponentModel;

namespace DB_Tool_FW
{
    public interface IDB_Manager
    {

        DataModel ReadDevData(int porzessId);
        void WriteDevData(DataModel dataModel);
        void DeleteAll();
        int ReadLastProzessId();
        void WriteProzessData(ProzessModel prozessModel);
        ProzessModel ReadProzessFromDb(int prozessId);
        bool SetUpDbConnection(string path);
        string CheckDbConnection();
        bool CreateDb(string dB_path);
        List<int> FindProzess(string searchValue);
        BindingList<string> ReadDataDesignations();
        List<int> FindDevData(string comparisonSymbol, string designation, string searchValue);
    }
}