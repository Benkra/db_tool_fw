﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Net.Http.Headers;
using DB_Tool_FW.DataManagement;
using System.Windows.Documents;
using System.Drawing.Text;
using DB_Tool_FW.DB_and_Data;
using DB_Tool_FW.Models;
using FluentAssertions.Formatting;
using System.ComponentModel;

namespace DB_Tool_FW

{
    public sealed class DB_Manager : IDB_Manager
    {
        //Fields

        private static DB_Manager instance;
        private Enums.Tables table;

        private SQLiteConnection sqliteConnection;
        private string path; //@"C:\Users\bschwaiger\Desktop\Facharbeit\Eigene Datenbanken\DeviceDB_1.0.db";
        private string sqliteConnectionString;
        //const string FILE_NAME = "Database.sqlite";

        private List<List<Object>> matrixList;
        private bool dbFileExists;

        private string initFilePath = Environment.CurrentDirectory + @"\ini.txt";

        


        //Constructor
        private DB_Manager()
        {
            if (File.Exists(initFilePath))
            {
                using (StreamReader sr = new StreamReader(initFilePath))
                {
                    path = sr.ReadToEnd();

                }
                if (path != string.Empty)
                {
                    DbFileExists = true;
                    sqliteConnectionString = "Data Source=" + path + ";Version=3;";

                }
                

            }
        }

        //Sinleton
        public static DB_Manager Instanz
        {
            get
            {
                if (instance == null)
                {
                    if (instance == null)
                    {
                        instance = new DB_Manager();
                    }

                }
                return instance;
            }
        }
       
        public bool DbFileExists { get => dbFileExists; private set => dbFileExists = value; }


        #region Database manipulation
        public bool CreateDb(string dB_path)
        {
            bool validConnection = true;
            sqliteConnectionString = "Data Source=" + dB_path + ";Version=3;";
            
            ProzessModel prozessModel = new ProzessModel();
            DataModel dataModel = new DataModel();

            if (!File.Exists(initFilePath))
            {
                using (StreamWriter sw = File.CreateText(initFilePath))
                {
                    sw.WriteLine(dB_path);
                }

            }


            string[] query = new string[2];

            query[0] = prozessModel.GetTableCreationString();
            query[1] = dataModel.GetTableCreationString();

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    validConnection = false;
                    throw new Exception("Probleme beim Erstellen der Datenbank aufgetreten... " + '\r' + '\n' + e.Message);
                   
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {

                    foreach (var item in query)
                    {
                        sqlCommand.CommandText = item;
                        sqlCommand.ExecuteNonQuery();
                    }
                }


                    using (StreamWriter sw = new StreamWriter(initFilePath, false))
                    {
                        sw.WriteLine(dB_path);
                    }

                    path = dB_path;
            }

                return validConnection;
        }

        public bool SetUpDbConnection(string dB_path)
        {
            bool validConnection = false;
            sqliteConnectionString = "Data Source=" + dB_path + ";Version=3;";

            if (!File.Exists(initFilePath))
            {
                using (StreamWriter sw = File.CreateText(initFilePath))
                {
                    sw.WriteLine(dB_path);
                }

            }
            else
            {
                using (StreamWriter sw = new StreamWriter(initFilePath, false))
                {
                    sw.WriteLine(dB_path);
                }
            }

            path = dB_path;

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();
                    if (CheckDbConnection() != string.Empty)
                    {
                        validConnection = true;
                    }

                }
                catch
                {
                    
                }


            }
            return validConnection;
        }

        /// <summary>
        /// Retunrs the DB path if the DB exists otherwise an empty string
        /// </summary>
        /// <returns></returns>
        public string CheckDbConnection()
        {

            string dbPath = path;
            SQLiteConnection sqliteConnection;
            string query = "SELECT* FROM Vorgang;";

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                if (File.Exists(dbPath))
                {
                    try
                    {
                        sqliteConnection.Open();
                        

                    }
                    catch
                    {
                        dbPath = string.Empty;
                    }

                    using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                    {
                        sqlCommand.CommandText = query;
                        try
                        {
                            SQLiteDataReader myReader = sqlCommand.ExecuteReader();
                            while (myReader.Read()) ;

                        }
                        catch (Exception)
                        {

                            dbPath = string.Empty;
                        }
                        
                        

                    }
                       
                }
                else
                {
                    dbPath = string.Empty;
                }




            }

            return dbPath;

        }

        #endregion


        #region writing data to DB

        /// <summary>
        /// The data of the CurrentDataModel insance gets written in to the Database 
        /// </summary>
        /// <param name="dataModel"></param>
        /// <exception cref="Exception"></exception>
        public void WriteDevData(DataModel dataModel)
        {
            List<string> querys = new List<string>();
            List<RawDataModel> rawDataModels = dataModel.RawDataModels;

            int prozessId = (int) dataModel.ProzessId;

            foreach (var rawDataModel in rawDataModels)
            {
                querys.Add("INSERT INTO DevData ("
                    + rawDataModel.GetItemsDbDesignationString(',') + 
                    ") VALUES (" 
                    + rawDataModel.GetItemsValueString(',') + ");");


                //+ rawDataModel.DataType +  /DataType,
            }

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim schreiben der Dev Danten von " +
                       "der Datenbank..." + e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {

                    foreach (var query in querys)
                    {
                        sqlCommand.CommandText = query;
                        sqlCommand.ExecuteNonQuery();

                    }

                }




            }

        }

        /// <summary>
        /// Writes the data from the 
        /// </summary>
        /// <param name="prozessModel"></param>
        /// <exception cref="Exception"></exception>
        public void WriteProzessData(ProzessModel prozessModel)
        {

            string query = "INSERT INTO Vorgang (" 
                + prozessModel.GetItemsDbDesignationString(",") +
                ") VALUES (" 
                + prozessModel.GetItemsValueString(',') + 
                ");";


            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim schreiben der Prozess Danten von " +
                        "der Datenbank..." + '\r' + '\n' + e.Message);
                }


                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    sqlCommand.ExecuteNonQuery();

                }


            }
        }

        /// <summary>
        /// !!!!!!!!!!!!Deletes ALL device data in the DB !!!!!!!!
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void DeleteAll()
        {
            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("An eror ocured: " + e.ToString());
                }

                SQLiteCommand sqlCommand = sqliteConnection.CreateCommand();
                string query = "DELETE FROM DevData;";


                sqlCommand.CommandText = query;
                sqlCommand.ExecuteNonQuery();




            }
        }

        #endregion



        #region Reading Data from Db

        /// <summary>
        /// REads the highest intPorozessId from DB and returns the value
        /// </summary>
        /// <returns></returns>
        public int ReadLastProzessId()
        {
            ProzessModel prozessModel = new ProzessModel();
            
            int maxProzessId = -1;

            string query = "SELECT MAX(" +
                "\"" + prozessModel.ProzessId.ItemDbDesignation + "\"" +
                ") FROM Vorgang;";

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    Debug.WriteLine("An eror ocured: " + e.ToString());
                }

                SQLiteCommand sqlCommand = sqliteConnection.CreateCommand();
                sqlCommand.CommandText = query;
                SQLiteDataReader myReader = sqlCommand.ExecuteReader();


                object[] values = new object[myReader.FieldCount];


                while (myReader.Read())
                {
                    myReader.GetValues(values);
                    try
                    {
                        maxProzessId = Convert.ToInt32(values[0]);

                    }
                    catch (Exception)
                    {

                        maxProzessId = 0;
                    }

                }

            }



            return maxProzessId;
        }
        /// <summary>
        /// Reads alle records form the table with prozessIds given in the parameter
        /// It returns a instance of type DataModel
        /// </summary>
        /// <param name="prozessId"></param>
        /// <returns></returns>
        public DataModel ReadDevData(int prozessId)
        {

            DataModel dataModel = new DataModel(prozessId);
            List<RawDataModel> rawDataModels = new List<RawDataModel>();


            string query = "SELECT Vorgangs_id, Numberator, Designation, Value, DataKind" +
                " FROM DevData WHERE Vorgangs_id= " + prozessId;

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim Lesen der Device Danten von " +
                        "der Datenbank... " + '\r' + '\n' + e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    SQLiteDataReader myReader = sqlCommand.ExecuteReader();

                    object[] values = new object[myReader.FieldCount];


                    while (myReader.Read())
                    {
                        RawDataModel rawData = new RawDataModel();
                        myReader.GetValues(values);

                        rawData.Add(values);
                        dataModel.AddRawDataModel(rawData);

                    }

                }


            }

            return dataModel;

        }

        /// <summary>
        /// Returns a List of the ProzessIds where the Prozesses contain the string value of the parameter
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<int> FindProzess(string searchValue)
        {
            ProzessModel prozessModel = new ProzessModel();
            List<int> prozessIds = new List<int>();

            string query = "SELECT Vorgangs_id" +
                " FROM Vorgang WHERE " +
                  prozessModel.GetItemsDbDesignationString("||") +
                " LIKE " + "\"" + searchValue + "\"" + ";";


            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim Lesen der Device Danten von " +
                        "der Datenbank... " + '\r' + '\n' + e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    SQLiteDataReader myReader = sqlCommand.ExecuteReader();

                    object[] values = new object[myReader.FieldCount];

                    while (myReader.Read())
                    {
                        RawDataModel rawData = new RawDataModel();
                        myReader.GetValues(values);

                        prozessIds.Add(Convert.ToInt32((long)values[0]));


                    }

                }

            }

            return prozessIds;
        }

        /// <summary>
        /// Returns a list of ProzessIds
        /// </summary>
        /// <param name="comparisonSymbol"></param>
        /// <param name="designation"></param>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<int> FindDevData(string comparisonSymbol, string designation, string searchValue)
        {

            List<int> prozessIds = new List<int>();
            string query = string.Empty;

            if (designation == null || designation == string.Empty || searchValue == null || searchValue == string.Empty)
            {
                return prozessIds;

            }

            switch (comparisonSymbol)
            {
                case "=":
                    {
                        if (designation != null)
                        {
                            query = "SELECT *" +
                                    " FROM DevData WHERE Designation = " + "\"" + designation + "\"" +
                                    " AND Value LIKE " + "\"" + searchValue + "\"" + ";";
                        }

                        break;
                    }

                case ">":
                    {
                        if (designation != null)
                        {
                            query = "SELECT *" +
                                    " FROM DevData WHERE Designation = " + "\"" + designation + "\"" +
                                    " AND Value < " + searchValue + ";";
                        }

                        break;
                    }

                case "<":
                    {
                        if (designation != null)
                        {
                            query = "SELECT *" +
                                    " FROM DevData WHERE Designation = " + "\"" + designation + "\"" +
                                    " AND Value > " + searchValue + ";";
                        }

                        break;
                    }
                default:
                    break;
            }



            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim Lesen der Device Danten von " +
                        "der Datenbank... " + '\r' + '\n' + e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    SQLiteDataReader myReader = sqlCommand.ExecuteReader();

                    object[] values = new object[myReader.FieldCount];

                    while (myReader.Read())
                    {
                        RawDataModel rawData = new RawDataModel();
                        myReader.GetValues(values);

                        prozessIds.Add(Convert.ToInt32((long)values[0]));


                    }

                }

            }

            return prozessIds;
        }

        /// <summary>
        /// Returns the ProzessModel instance with the prozessId
        /// </summary>
        /// <param name="prozessId"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public ProzessModel ReadProzessFromDb(int prozessId)
        {
            ProzessModel prozessModel = new ProzessModel(prozessId);

            string query = "SELECT " + prozessModel.GetItemsDbDesignationString(",") +
                "FROM Vorgang " +
                "WHERE Vorgangs_id= + " +
                prozessId + ";";

            SQLiteConnection sqliteConnection;


            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception("Probleme beim lesen der Prozess Danten von " +
                       "der Datenbank..." + '\r' + '\n' + e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    SQLiteDataReader myReader = sqlCommand.ExecuteReader();

                    object[] values = new object[myReader.FieldCount];


                    while (myReader.Read())
                    {
                        myReader.GetValues(values);
                        prozessModel.Add(values);
                    }
                }


            }

            return prozessModel;
        }

        /// <summary>
        /// Returns a BindingList with all designations of the DevData. Like... "CommPathSlaveIdx"; "_DevData.dataset"; ..
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public BindingList<string> ReadDataDesignations()
        {   

            BindingList<string> designations = new BindingList<string>();

            if (CheckDbConnection()== string.Empty)
            {
                return designations;
            }


            string query = "SELECT Designation FROM DevData;";

            SQLiteConnection sqliteConnection;

            using (sqliteConnection = new SQLiteConnection(sqliteConnectionString))
            {
                try
                {
                    sqliteConnection.Open();

                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

                using (SQLiteCommand sqlCommand = sqliteConnection.CreateCommand())
                {
                    sqlCommand.CommandText = query;
                    SQLiteDataReader myReader = sqlCommand.ExecuteReader();

                    object[] values = new object[myReader.FieldCount];


                    while (myReader.Read())
                    {
                        myReader.GetValues(values);

                        if (values != null && !designations.Contains(values[0].ToString()))
                        {
                            designations.Add(values[0].ToString());
                        }


                    }

                }


            }



            return designations;

        }


        #endregion




    }
}