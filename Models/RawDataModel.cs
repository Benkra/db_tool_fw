﻿using DB_Tool_FW.Models;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace DB_Tool_FW.DataManagement
{
    public class RawDataModel
    {


        private Item prozessId = new Item("prozessId", "Vorgangs_id", "int");
        private Item numberator = new Item("numberator", "Numberator", "int");
        private Item designation = new Item("designation", "Designation", "string");
        private Item value = new Item("value", "Value", "string");
        private Item dataKind = new Item("dataKind", "DataKind", "int");

        public SortedList<int, Item> DataPropertys = new SortedList<int, Item>();

        //Properties

        public Item ProzessId { get => prozessId; set => prozessId = value; }
        public Item Numberator { get => numberator; set => numberator = value; }
        public Item DataKind { get => dataKind; set => dataKind = value; }
        public Item Designation { get => designation; set => designation = value; }
        public Item Value { get => value; set => this.value = value; }


        public RawDataModel() 
        {   
            AddPropertysToList();
        }

        private void AddPropertysToList()
        {
            DataPropertys.Add(1, ProzessId);
            DataPropertys.Add(2, Numberator);
            DataPropertys.Add(3, DataKind);
            DataPropertys.Add(4, Designation);
            DataPropertys.Add(5, Value);

        }


        public string GetItemsValueString(char sybol)
        {

            string itemValuesString = string.Empty;

            for (int i = 1; i <= DataPropertys.Count; i++)
            {
                if (i == DataPropertys.Count)
                {
                    sybol = ' ';
                }

                string temp = DataPropertys[i].ItemValue.ToString();
                if (temp == "")
                {
                    itemValuesString = itemValuesString + "\"" + "\"" + sybol;
                }
                else
                {
                    if (DataPropertys[i].ItemType == "string")
                    {
                        itemValuesString = itemValuesString + "\"" + DataPropertys[i].ItemValue.ToString() + "\"" + sybol;
                    }
                    else
                    {
                        itemValuesString = itemValuesString + DataPropertys[i].ItemValue.ToString() + sybol;
                    }

                }

            }
            return itemValuesString;
        }

        public string GetItemsDbDesignationString(char sybol)
        {

            string itemsDbDesignationString = string.Empty;

            for (int i = 1; i <= DataPropertys.Count; i++)
            {
                if (i == DataPropertys.Count)
                {
                    sybol = ' ';
                }

                itemsDbDesignationString = itemsDbDesignationString + DataPropertys[i].ItemDbDesignation + sybol + " ";
            }

            return itemsDbDesignationString;
        }

        public void WriteProperties(int prozessId, int readNumberator, string designation, string value, int dataKind)
        {
            ProzessId.ItemValue = prozessId;
            Numberator.ItemValue = readNumberator;
            Designation.ItemValue = designation;
            Value.ItemValue = value;
            DataKind.ItemValue = dataKind;

        }

        public void Add(object[] data)
        {
            ProzessId.ItemValue = data[0]; // (int)(short) data[0];
            Numberator.ItemValue = data[1];
            Designation.ItemValue = data[2];
            Value.ItemValue = data[3]; 
            DataKind.ItemValue = data[4];

            //DataKind.ItemValue = Convert.ToInt32(data[4]);


        }

        public override string ToString()
        {
            string sRawdadamodel, sKind;


            if ((int)DataKind.ItemValue == 1)
            {
                sKind = "PublicData";
            }
            else if ((int)DataKind.ItemValue == 2)
            {
                sKind = "Counter";
            }
            else
            {
                sKind = "sRomVariables";
            }


            sRawdadamodel = " intPorozessId: " + ProzessId.ItemValue + " Auslesung: "+ Numberator.ItemValue + " Bezeichnung: " + Designation + " Wert: " + Value + " Daten: " + sKind;

            return sRawdadamodel;
        }

        public string GetTableCreationString()
        {
            string TableCreationString = "" +
                "CREATE TABLE DevData " +
                "(" +
                ProzessId.ItemDbDesignation + " INTEGER NOT NULL, " +
                Numberator.ItemDbDesignation + " TEXT NOT NULL," +
                DataKind.ItemDbDesignation + " TEXT," +
                Designation.ItemDbDesignation + " TEXT," +
                Value.ItemDbDesignation + " TEXT" +
                ");";


            return TableCreationString;
        }


    }
}

