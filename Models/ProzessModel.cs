﻿using DB_Tool_FW.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace DB_Tool_FW.DB_and_Data
{
    public class ProzessModel
    {

        //Fields
        private Item prozessId = new Item("prozessId", "Vorgangs_id", "int");
        private Item capturer = new Item("capturer", "Erfasser", "string");
        private Item receiptDateRnD = new Item("receiptDateRnD", "Eingangsdatum", "string");
        private Item rkNumber = new Item("rkNumber", "RK_Nummer", "string");
        private Item _PHI = new Item("_PHI", "PHI", "string");
        private Item itemNumber = new Item("itemNumber", "ArtikelNr", "string");
        private Item contactPersonSV = new Item("contactPersonSV", "Ansprechpartner_SV", "string");
        private Item trader = new Item("trader", "Händler", "string");
        private Item endCustomer = new Item("endCustomer", "Endkunde", "string");
        private Item door = new Item("door", "Tür", "string");
        private Item history = new Item("history", "Historie", "string");
        private Item errorDescription = new Item("errorDescription", "Fehlerbeschreibung", "string");

        public int intPorozessId;

        //List of all the Items
        public SortedList<int, Item> ProzessPropertys = new SortedList<int, Item>();
        
        string searchablItemsString;




        //Propertys
        public Item ProzessId { get => prozessId; set => prozessId = value; }
        public Item Capturer { get => capturer; set => capturer = value; }
        public Item ReceiptDateRnD { get => receiptDateRnD; set => receiptDateRnD = value; }
        public Item RkNumber { get => rkNumber; set => rkNumber = value; }
        public Item PHI { get => _PHI; set => _PHI = value; }
        public Item ItemNumber { get => itemNumber; set => itemNumber = value; }
        public Item ContactPersonSV { get => contactPersonSV; set => contactPersonSV = value; }
        public Item Trader { get => trader; set => trader = value; }
        public Item EndCustomer { get => endCustomer; set => endCustomer = value; }
        public Item Door { get => door; set => door = value; }
        public Item History { get => history; set => history = value; }
        public Item ErrorDescription { get => errorDescription; set => errorDescription = value; }




        //Constructor
        public ProzessModel(int prozessId)
        {
            this.ProzessId.ItemValue = prozessId;
            intPorozessId = prozessId;
            AddPropertysToList();

        }

        public ProzessModel()
        {
            AddPropertysToList();
        }


        private void AddPropertysToList()
        {
            ProzessPropertys.Add(1, ProzessId);
            ProzessPropertys.Add(2, Capturer);
            ProzessPropertys.Add(3, ReceiptDateRnD);
            ProzessPropertys.Add(4, RkNumber);
            ProzessPropertys.Add(5, PHI);
            ProzessPropertys.Add(6, ItemNumber);
            ProzessPropertys.Add(7, ContactPersonSV);
            ProzessPropertys.Add(8, Trader);
            ProzessPropertys.Add(9, EndCustomer);
            ProzessPropertys.Add(10, Door);
            ProzessPropertys.Add(11, History);
            ProzessPropertys.Add(12, ErrorDescription);
        }

        public string GetItemsValueString(char sybol)
        {

            string itemValuesString =string.Empty;
       
            for (int i = 1; i <= ProzessPropertys.Count; i++)
            {
                if (i == ProzessPropertys.Count)
                {
                    sybol = ' ';
                }

                string temp = ProzessPropertys[i].ItemValue.ToString();
                if (temp == "")
                {
                    itemValuesString = itemValuesString + "\"" + "\"" + sybol;
                }
                else
                {
                    if (ProzessPropertys[i].ItemType == "string")
                    {
                        itemValuesString = itemValuesString + "\"" + ProzessPropertys[i].ItemValue.ToString() +"\"" + sybol;
                    }
                    else
                    {
                        itemValuesString = itemValuesString + ProzessPropertys[i].ItemValue.ToString() + sybol;
                    }
                    
                }
                
            }
            //itemValuesString = itemValuesString + ProzessPropertys[ProzessPropertys.Count - 1].ItemValue.ToString();

            return itemValuesString;
        }

        public string GetItemsDbDesignationString(string sybols)
        {

            string itemsDbDesignationString = string.Empty;

            for (int i = 1; i <= ProzessPropertys.Count; i++)
            {
                if (i == ProzessPropertys.Count)
                {
                    sybols = " ";
                }

                itemsDbDesignationString = itemsDbDesignationString + ProzessPropertys[i].ItemDbDesignation + sybols + " ";
            }

            return itemsDbDesignationString;
        }

        public void Add(object[] prozessItemValues)
        {
            if (prozessItemValues.Length != ProzessPropertys.Count)
            {
                throw new Exception("Die Anzahl der Element stimmt nicht mit der Anzahl " +
                    "der Felder überein!");
            }

            for (int i = 0; i < prozessItemValues.Length; i++)
            {
                ProzessPropertys[i+1].ItemValue = prozessItemValues[i].ToString();
            }

        }

        public override string ToString()
        {
            string sContent = string.Empty;

            foreach (var content in ProzessPropertys)
            {
                sContent += content.Value.ItemDbDesignation.ToString() + " =  " +
                    content.Value.ItemValue.ToString() + '\r' + '\n';

            }


            return sContent;
        }

        public string ToStringInline()
        {
            string sContent = string.Empty;

            foreach (var content in ProzessPropertys)
            {
                sContent += content.Value.ItemDbDesignation.ToString() + " =  " +
                    content.Value.ItemValue.ToString() + "  ";

            }

            return sContent;
        }

        public string GetTableCreationString()
        {
            string TableCreationString = "" +
                "CREATE TABLE Vorgang " +
                "(" +
                "Vorgangs_id INTEGER NOT NULL PRIMARY KEY, " +
                "Erfasser TEXT NOT NULL," +
                "Eingangsdatum TEXT," +
                "RK_Nummer TEXT," +
                "PHI TEXT," +
                "ArtikelNr TEXT," +
                "Ansprechpartner_SV TEXT," +
                "Händler TEXT," +
                "Endkunde TEXT," +
                "Tür TEXT," +
                "Historie TEXT," +
                "Fehlerbeschreibung TEXT" +
                ");";


            return TableCreationString;
        }

        
    }
}
