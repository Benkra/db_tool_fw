﻿using DB_Tool_FW.DataManagement;
using DB_Tool_FW.DB_and_Data;
using DB_Tool_FW.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace DB_Tool_FW
{
    public class DataModel
    {
        //Fields
        private int maxNumberator;
        private string dataType;
        private int prozessId;



        private List<RawDataModel> rawDataModels;

        //Propertie
        public List<RawDataModel> RawDataModels { get => rawDataModels; }
        public int MaxNumberator { get => maxNumberator; set => maxNumberator = value; }
        public int ProzessId { get => prozessId; set => prozessId = value; }



        //Constructor
        public DataModel(int prozessId)
        {
            ProzessId = prozessId;

        }

        public DataModel()
        {


        }

     

        /// <summary>
        /// Adds the rawDataModels instanze to a List Property
        /// </summary>
        /// <param name="rawDataModel"></param>
        public void AddRawDataModel(RawDataModel rawDataModel)
        {
            if (RawDataModels==null) 
            {
                rawDataModels = new List<RawDataModel>();
            }

            if (Convert.ToInt32(rawDataModel.Numberator.ItemValue)>MaxNumberator)
            {
                MaxNumberator = Convert.ToInt32( rawDataModel.Numberator.ItemValue);

            }

            ProzessId =Convert.ToInt32( rawDataModel.ProzessId.ItemValue);

            //DataType = rawDataModel.DataType;
            foreach (RawDataModel rawModel in rawDataModels)
            {
                if (rawModel.Numberator == rawDataModel.Numberator)
                {
                    if (rawModel.Designation == rawDataModel.Designation)
                    {
                        if (rawModel.Value == rawDataModel.Value)
                        {
                            return;

                        }

                    }

                }
            }

            rawDataModels.Add(rawDataModel);

        }

        /// <summary>
        /// Returns a Dictionary  of Designation/Value pairs with date was read while the readNumbarator was 
        /// was equal to the paramter.
        /// </summary>
        /// <param name="readNumberator"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetDesignationValues(int readNumberator)
        {
            //ArrayList with the size of MaxNumberator
            List<RawDataModel>[] rawDataModelArray = new List<RawDataModel>[MaxNumberator];    
            Dictionary<string, object> dataDictionary = new Dictionary<string, object>();

            //Fills the ArrayList with RawDataModel Lists
            for (int i = 0; i < rawDataModelArray.Length; i++)
            {
                rawDataModelArray[i] = new List<RawDataModel>();
            }
            //Adds to every RawDataModel List the RawDataModels with equal ReadNumberator
            foreach (RawDataModel raw in RawDataModels)
            {
                rawDataModelArray[Convert.ToInt32(raw.Numberator.ItemValue) - 1].Add(raw);
            }

            //Every postion of the ArrayList smaler/equals the MaxNumberator
            for (int i = MaxNumberator-1; i >= 0; i--)
            {
                foreach (var item in rawDataModelArray[i])
                {
                    if (dataDictionary.ContainsKey((string)item.Designation.ItemValue))
                    {
                        break;
                    }
                    dataDictionary.Add((string)item.Designation.ItemValue, item.Value.ItemValue);

                }



            }

            return dataDictionary;

        }

        /// <summary>
        /// Returns a list with all the RawDataModels
        /// </summary>
        /// <returns></returns>
        public List<RawDataModel> GetDataModel()
        {
            return RawDataModels;
        }

        /// <summary>
        /// Adds a List of RawDataModels to the list
        /// </summary>
        /// <param name="rawDataModels"></param>
        public void AddDataModel(List<RawDataModel> rawDataModels)
        {
            if(rawDataModels == null)
            {
                rawDataModels = new List<RawDataModel>();
            }
            else
            {
                foreach (var item in rawDataModels)
                {
                    rawDataModels.Add(item);

                }
                
            }
       

        }

        /// <summary>
        /// Returns a string with all designation value pairs. Like.... "designation1 = value1 \n ..."
        /// </summary>
        /// <param name="readNumberator"></param>
        /// <returns></returns>
        public string GetDataString(int readNumberator)
        {
            string dataString =string.Empty;

            Dictionary<string, object> keyValuePairs =  GetDesignationValues(readNumberator);

            foreach (var item in keyValuePairs)
            {
                dataString = dataString + item.Key + "=" + item.Value + '\r' + '\n';

            }
            

            return dataString;
        }

        /// <summary>
        /// Returns a string with all designation value pairs. Like.... "designation1 = value1 \n ..." for Numberator = maxNumberator
        /// </summary>
        /// <returns></returns>
        public string GetDataString()
        {
            return GetDataString(maxNumberator);
        }

        /// <summary>
        ///  Returns a string with all designation value pairs. Like.... "designation1 = value1 ..."
        /// </summary>
        /// <param name="readNumberator"></param>
        /// <returns></returns>
        public string GetDataStringInline(int readNumberator)
        {
            string dataString = string.Empty;
            int i = 0;

            Dictionary<string, object> keyValuePairs = GetDesignationValues(readNumberator);

            foreach (var item in keyValuePairs)
            {
                dataString = dataString + item.Key + " = " + item.Value + ";   ";

                i++;

                if (i>20)
                {
                    dataString = dataString + '\r' + '\n';
                    i = 0;

                }

            }


            return dataString;
        }

        /// <summary>
        /// Returns a string with all designation value pairs. Like.... "designation1 = value1 ..." with Numberator = maxNumberator 
        /// </summary>
        /// <returns></returns>
        public string GetDataStringInline()
        {
            return GetDataStringInline(maxNumberator);
        }

       
        public string DebugToString()
        {

            string sModelString;

            sModelString = "ProzessID: " + ProzessId +'\r' +'\n';

            foreach (RawDataModel raw in RawDataModels)
            {
                sModelString += raw.ToString();
            }

            return sModelString;
        }

        public override string ToString()
        {
            return "Auslesung: " + maxNumberator;
        }

        /// <summary>
        /// Calls the methode GetTableCreatoinString() from a RawDataModel instance and returns the value
        /// </summary>
        /// <returns></returns>
        public string GetTableCreationString()
        {
            string TableCreationString = new RawDataModel().GetTableCreationString();


            return TableCreationString;
        }







    }
}
