﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_Tool_FW.Models
{
    public class Item
    {
        string itemName;
        string itemDbDesignation;
        string itemType;
        object itemValue;

        public string ItemName { get => itemName; private set => itemName = value; }
        public string ItemDbDesignation { get => itemDbDesignation; private set => itemDbDesignation = value; }
        public string ItemType { get => itemType; private set => itemType = value; }
        public object ItemValue { get => itemValue; set => itemValue = value; }

        public Item() 
        {
        }

        public Item(string itemName, string itemDbDesignation, string itemType)
        {
            this.ItemName = itemName;
            this.ItemDbDesignation = itemDbDesignation;
            this.ItemType = itemType;
        }

        public void SetItemNama(string itemName)
        {
            this.ItemName = itemName;

        }

        public void SetDbDesignation(string itemDbDesignation)
        {
            this.ItemDbDesignation= itemDbDesignation;  
        }


    }
}
