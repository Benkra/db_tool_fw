﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using DB_Tool_FW.DataManagement;
using InitG2ToolDrv;
using Cursor = System.Windows.Forms.Cursor;

namespace DB_Tool_FW

{

    public class SVDevCom : ISVDevCom
    {


        Dictionary<UInt32, String> m_LastRomVarStrs = new Dictionary<uint, string>(); // phiValue, RomVars
        Dictionary<UInt32, String> m_LastCntStrs = new Dictionary<uint, string>(); // phiValue, Counters
        ReadPublicClass m_publicdata = null;
        TraReadPublicClass m_traData = null;
        phiClass m_phi = null;

        private List<string> m_RawPubdatas;
        private Dictionary<string, string> m_AtributeValuePairs;
        private List<string> m_Attributes;

        private DataModel m_dataModel;


        string m_docPath = Environment.CurrentDirectory;
        string m_sPubData;


        public string sCommunicationPath { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public SVDevCom()
        {
            sCommunicationPath = "BLE";
           
        }


        /// <summary>
        /// Initiates a Public ReadDevData and callst the methode CreateAndGetDataModel , An Exceptioni is thrown if
        /// the response from ReadPublicAndShow() is different to 'OK'
        /// </summary>
        /// <exception cref="Exception"></exception>
        public DataModel ReadPublicData(int prozessId, int readNumberator)
        {
            //m_RawPubdatas = DevService.readPubDataFromTxt("PubData_SHAX.txt", m_docPath);


            string response = ReadPublicAndShow().ToString();

            if (!response.Equals("OK"))
            {
                throw new Exception("Beim Auslesen ist ein Problem aufgetreten....: " + response);
            }


            m_RawPubdatas = DevService.linesToList(m_sPubData);


            m_Attributes = DevService.AttributesToList(m_RawPubdatas);
            m_AtributeValuePairs = DevService.AtributeValuePairsToDic(m_Attributes, m_RawPubdatas);
            return CreateAndGetDataModel(prozessId, readNumberator);
 

        }

        /// <summary>
        /// Creates from the m_AtributeValuePairs Property a DataModel instance
        /// </summary>
        /// <param name="prozessId"></param>
        /// <param name="readNumberator"></param>
        /// <returns></returns>
        public DataModel CreateAndGetDataModel(int prozessId, int readNumberator)
        {
            
            DataModel dataModel = new DataModel();
            RawDataModel rawDataModel = new RawDataModel();

            foreach (var item in m_AtributeValuePairs)
            {
                rawDataModel = new RawDataModel();
                rawDataModel.WriteProperties(prozessId, readNumberator, item.Key, item.Value, 1);
                Console.WriteLine("");
                dataModel.AddRawDataModel(rawDataModel);
            }

            return dataModel;
        }


        /// <summary>
        /// Prints SmartCD FW
        /// </summary>
        public void GetSmartCDFW()
        {
            try
            {
                driverResultEnum result = Device.ReadSmartCD_FW(sCommunicationPath, out string smartCDFW);
                Device.ReadPublic(sCommunicationPath, ref m_publicdata);
                Console.WriteLine(m_publicdata.ToString());
            }
            finally
            {
                Device.CloseCom();

            }


        }



        #region coppied_from_"InitG2ToolTest"

        private string ReadPublicAndShow()
        {
            driverResultEnum result = driverResultEnum.Err_UnknownError;
            try
            {
                if (Device.CommunicationPathIs(sCommunicationPath, "G2,!TRA"))
                {
                    try
                    {
                        result = Device.TraReadPublic(sCommunicationPath, 0, out m_traData);
                        if (result != driverResultEnum.OK)
                        {
                            return result.ToString();
                        }

                        result = Device.TraReadDevData(ref m_traData);
                        if (result != driverResultEnum.OK)
                        {
                            return result.ToString();
                        }

                    }
                    finally
                    {
                        Device.CloseCom();
                    }
                }
                else
                {
                    // Make ReadPublic with a copy to keep it in case of error
                    ReadPublicClass rp = m_publicdata;

                    result = ReadPublic(ref rp, true);

                    if (result != driverResultEnum.OK)
                    {
                        return result.ToString();
                    }

                    m_publicdata = rp; // Take new class

                    // Take Master for output here
                    if (m_publicdata.Master != null)
                    {
                        m_publicdata = m_publicdata.Master;
                    }

                    //
                    m_sPubData = PubDataAsString(m_publicdata);
                    //Console.WriteLine(sPubdata);
                    //


                    //if (publicdata.Is_AXEOS())
                    //{
                    //    if ((publicdata.ProgState_DrvVer > ProgState_Enum._50_HAS_APP) &&
                    //        (publicdata.StateRaw & 0x80/*=localKeyError/isCryptError*/) != 0)
                    //    {

                    //    }
                    //if (publicdata.DevData != null &&
                    //    publicdata.DevData.progCounter_Max > 1000 &&
                    //    publicdata.DevData.progCounter_Max < 10000)
                    //{
                    //    WriteLine($"\r\nWARNING: ProgCounter_Max = {publicdata.DevData.progCounter_Max}");
                    //    if (String.IsNullOrWhiteSpace(DebugLineTextBox.Text))
                    //    {
                    //        DebugLineTextBox.Text = "2006 0 // To set ProgCounter_Max=0";
                    //    }
                    //}
                }
                //}
            }
            finally
            {

            }
            return result.ToString();
        }
        private driverResultEnum ReadPublic(ref ReadPublicClass publicdata, bool readDevData)
        {
            driverResultEnum result = driverResultEnum.Err_UnknownError;

            string communicationPath = sCommunicationPath;
            // If communication path is fast, read also DevData
            if (Device.CommunicationPathIs(communicationPath, "USB") ||
                Device.CommunicationPathIs(communicationPath, "MP") ||
                Device.CommunicationPathIs(communicationPath, "BLE") ||
                Device.CommunicationPathIs(communicationPath, "FTDI"))
            {
                readDevData = true;
            }

            try
            {
                // If communicationPath has no PHI and there is publicdata take the PHI from publicdata
                Device.SeperateCommPathSlaveIdx(communicationPath, out string communicationPathWithoutPHI, out int slaveIdxPhi);
                if (publicdata != null && publicdata.PHIval != 0)
                {
                    if (slaveIdxPhi == 0)
                    {
                        communicationPath = $"{publicdata.PHI}@{communicationPathWithoutPHI}";
                        Device.SeperateCommPathSlaveIdx(communicationPath, out communicationPathWithoutPHI, out slaveIdxPhi);
                    }
                }
                //// BLE-Scan if PHI is unknown via BLE
                //if (Device.CommunicationPathIs(communicationPath, "BLE"))
                //{   // Do ReadPublic with BLE-Scan
                //    result = (new BLEScanForm(expertMode: true)).ShowDialog(
                //        this, communicationPath, out publicdata,
                //        "Select device to read.", "&OK");
                //}
                else
                {
                    publicdata = null; // Clear old currentDataModel first
                    result = Device.ReadPublic(communicationPath, ref publicdata, readDevData);
                }
                if (result != driverResultEnum.OK) return result;

            }
            finally
            {
                Device.CloseCom();

                //// Get SlaveIdxPhi and corresponding publicdata from publicdata
                //publicdata = GetCorrespPubData(communicationPath, publicdata);
                //if (publicdata is null)
                //{
                //    WriteLine("\r\nERROR: Got no PublicData from '" + communicationPath + "'.");
                //    if (result == driverResultEnum.OK) { result = driverResultEnum.Err_UnknownError; }
                //}
            }

            return result;
        }
        public string PubDataAsString(ReadPublicClass pubdata)
        {
            bool skipStdValues = false; // Skip standard values if SHIFT is not pressed
            if (pubdata is null)
            {
                return "   Nothing";
            }
            else
            {
                String result = $"Master ({pubdata.ProductName}):\r\n";  //:{pubdata.PHI}@{pubdata.CommPathSlaveIdx}
                while (pubdata != null)
                {
                    bool bSkipDevData = false;
                    //result += pubdata.DebugToString();
                    //if (pubdata.DevData != null)
                    //{ result += pubdata.DevData.ToStringDecoded(true); }


                    ///if Shift key is pressed and (product Name is SR30GWM_SV ore product name is SOM) and DevDAta != null and phi != null
                    ///
                    if (skipStdValues &&
                        (pubdata.ProductName == "SR30GWM_SV" || pubdata.ProductName == "SOM") &&
                        pubdata.DevData != null &&
                        pubdata.PHIval != pubdata.DevData.PHIvalue)
                    { // "virtual" Slave from SREL3
                        bSkipDevData = true;
                        result += "\r\n" +
                         "CommPathSlaveIdx=" + pubdata.CommPathSlaveIdx.ToString() + "\r\n" +
                         "PHI=" + pubdata.PHI + " dez=" + pubdata.PHIval.ToString() + " hex=0x" + pubdata.PHIval.ToString("X8") + (pubdata.DevData is null ? "" :
                                CheckDevData("PHI", (pubdata.DevData != null ? new phiClass((int)pubdata.DevData.PHIvalue).PHIStr : pubdata.PHI), pubdata.PHI)) +
                                (pubdata.DevData != null && pubdata.DevData.deviceIdentifier != 0 ? "   ChipID: 0x" + pubdata.DevData.deviceIdentifier.ToString("X16") : "") + "\r\n" +
                         "SID=" + pubdata.SID.ToString() + " LID=" + pubdata.LID.ToString() + "\r\n" +
                            ((pubdata.DevData is null || pubdata.DevData.progState == (byte)pubdata.ProgState_DrvVer) &&
                             (pubdata.ProgState_DrvVer == (pubdata.LID < 16 ? ProgState_Enum._50_HAS_APP : ProgState_Enum._100_AREA_COMPLETE)) ? "" : "ProgState=" + pubdata.ProgState_DrvVer.ToString() +
                                (pubdata.Is_AXEOS() && pubdata.DevData != null ?
                                CheckDevData("progState", pubdata.DevData.progState.ToString(), ((byte)pubdata.ProgState_DrvVer).ToString()) : "") + "\r\n") +
                         "DeviceClass=" + pubdata.DeviceClass.ToString() + (pubdata.DevData is null ? "" :
                                (pubdata.Is_AXEOS() ?
                                CheckDevData("appClass", (pubdata.DevData != null ? pubdata.DevData.appClass : pubdata.DeviceClass).ToString(), pubdata.DeviceClass.ToString()) :
                                CheckDevData("deviceClass", (pubdata.DevData != null ? pubdata.DevData.deviceClass : pubdata.DeviceClass).ToString(), pubdata.DeviceClass.ToString()))) +
                                "/" + pubdata.DeviceSubclass.ToString("d2") + "ProductName=" + pubdata.ProductName + "\r\n" +
                         (pubdata.StateLSM == 0 ? "" : "   State=" + pubdata.StateLSM.ToString() + "\r\n") +
                         (skipStdValues && pubdata.StateRaw == 0 ||
                         (skipStdValues && pubdata.StateRaw == 0x80 && pubdata.ProgState_DrvVer != ProgState_Enum._100_AREA_COMPLETE) ?
                            "" : "   StateRaw=" + pubdata.StateRawToString() + "\r\n") +
                         (pubdata.ConfigRaw == 0 ? "" : "ConfigRaw=" + pubdata.ConfigRaw.ToString() + "\r\n") +
                         (pubdata.NextDevice == 0 ? "" : "NextDevice=" + pubdata.NextDevice.ToString() + "\r\n") +
                         (pubdata.LocalSlaveAddress == 0 ? "" : "LocalSlaveAddress=" + pubdata.LocalSlaveAddress.ToString() + "\r\n") +
                         "";
                    }


                    ///////////DAS NEHM ICH!!!!!///////////
                    else
                    {
                        result += "\r\n" +
                         "CommPathSlaveIdx=" + pubdata.CommPathSlaveIdx.ToString() + "\r\n" +
                         "Time=" + pubdata.Time.ToString("dd.MM.yyyy HH:mm:ss") + "\r\n" +
                         "DeviceVariantId=" + pubdata.DeviceVariantId.ToString() + (pubdata.DevData is null ? "" :
                                (pubdata.Is_AXEOS() ?
                                CheckDevData("appVariantID", pubdata.DevData.appVariantID.ToString(), pubdata.DeviceVariantId.ToString()) :
                                CheckDevData("variantID", pubdata.DevData.variantID.ToString(), pubdata.DeviceVariantId.ToString()))) +
                                GetTitleForDVI(pubdata.DeviceVariantId) +
                                "\r\n" +
                         "FW=" + pubdata.FW + "\r\n" +
                         "PHI=" + pubdata.PHI + " dez=" + pubdata.PHIval.ToString() + " hex=0x" + pubdata.PHIval.ToString("X8") + (pubdata.DevData is null ? "" :
                                CheckDevData("PHI", (pubdata.DevData != null ? new phiClass((int)pubdata.DevData.PHIvalue).PHIStr : pubdata.PHI), pubdata.PHI)) +
                                (pubdata.DevData != null && pubdata.DevData.deviceIdentifier != 0 ? "   ChipID: 0x" + pubdata.DevData.deviceIdentifier.ToString("X16") : "") + "\r\n" +
                         "SID=" + pubdata.SID.ToString() + "\r\n" +
                         " LID=" + pubdata.LID.ToString() + "\r\n" +
                         (pubdata.Is_AXEOS() ?
                            /* SID/LID-Check for AXEOS */
                            (skipStdValues &&
                                (pubdata.DevData is null || pubdata.DevData.progState == (byte)pubdata.ProgState_DrvVer) &&
                                (pubdata.ProgState_DrvVer == (pubdata.LID < 16 ? ProgState_Enum._50_HAS_APP : ProgState_Enum._100_AREA_COMPLETE))
                                ? "" : "   ProgState=" + pubdata.ProgState_DrvVer.ToString() +
                                    (pubdata.Is_AXEOS() && pubdata.DevData != null ?
                                    CheckDevData("progState", pubdata.DevData.progState.ToString(), ((byte)pubdata.ProgState_DrvVer).ToString()) : "") + "\r\n")
                            : /* SID/LID-Check for not AXEOS*/
                            "DrvVer=" + ((int)pubdata.ProgState_DrvVer) + "\r\n") +

                         "DeviceClass=" + pubdata.DeviceClass.ToString() + (pubdata.DevData is null ? "" :
                                (pubdata.Is_AXEOS() ?
                                CheckDevData("appClass", (pubdata.DevData != null ? pubdata.DevData.appClass : pubdata.DeviceClass).ToString(), pubdata.DeviceClass.ToString()) :
                                CheckDevData("deviceClass", (pubdata.DevData != null ? pubdata.DevData.deviceClass : pubdata.DeviceClass).ToString(), pubdata.DeviceClass.ToString()))) +
                                "/" + pubdata.DeviceSubclass.ToString("d2") + " ProductName=" + pubdata.ProductName + "\r\n" +
                         (skipStdValues && pubdata.StateLSM == 0 ? "" : "   State=" + pubdata.StateLSM.ToString() + "\r\n") +
                         (skipStdValues && pubdata.ProgCounter == 0 ? "" : "   DeviceType_ProgCounter=" + ((DeviceType_Enum)pubdata.ProgCounter).ToString() + "\r\n") +
                         (skipStdValues && pubdata.Sequence == 0 ? "" : $"   Sequence={pubdata.Sequence}" + (pubdata.Is_AXEOS() ? $" SE-FW=4.{pubdata.Sequence}" : "") + "\r\n") +
                         (skipStdValues && pubdata.EEPROM == 0 ? "" : "   EEPROM=" + pubdata.EEPROM.ToString() + (pubdata.Is_AXEOS() ? " PCB_Version='" + (char)(0x40 + pubdata.EEPROM) + "'" : "") + "\r\n") +
                         (skipStdValues && pubdata.LockLastAuthErr == 0 ? "" : "   LockLastAuthErr=" + pubdata.LockLastAuthErr.ToString() + "\r\n") +
                         (skipStdValues && pubdata.StateRaw == 0 ||
                         (skipStdValues && pubdata.StateRaw == 0x80 && pubdata.Is_AXEOS() && pubdata.ProgState_DrvVer != ProgState_Enum._100_AREA_COMPLETE) ?
                            "" : "   StateRaw=" + pubdata.StateRawToString() + "\r\n") +
                         (skipStdValues && pubdata.ConfigRaw == 0 ? "" : "   ConfigRaw=" + pubdata.ConfigRaw.ToString() + "\r\n") +
                         (skipStdValues && pubdata.NextDevice == 0 ? "" : "   NextDevice=" + pubdata.NextDevice.ToString() + "\r\n") +
                         (skipStdValues && pubdata.LocalSlaveAddress == 0 ? "" : "   LocalSlaveAddress=" + pubdata.LocalSlaveAddress.ToString() + "\r\n") +
                         "";
                    }

                    /// DevData nehme ich auch
                    /// 
                    if (pubdata.DevData != null && pubdata.Is_AXEOS() && bSkipDevData == false)
                    {
                        UInt16 logicalInputStates = pubdata.DevData.logicalInputStates;
                        result +=
                         (skipStdValues && pubdata.DevData.dataset == 0 ? "" : "_DevData.dataset=" + pubdata.DevData.dataset + "\r\n") +
                         "_DevData.deviceClass=" + pubdata.DevData.deviceClass + "\r\n" +
                         (skipStdValues && pubdata.DevData.variantID == 135 ? "" : "_DevData.variantID=" + pubdata.DevData.variantID + "\r\n") +
                         "_DevData.FWVersion=" + pubdata.DevData.FWVersion + "\r\n" +
                         (skipStdValues && pubdata.DevData.DrvVersion == 0 ? "" : "_DevData.DrvVersion=" + pubdata.DevData.DrvVersion + "\r\n") +
                         (skipStdValues && pubdata.DevData.isCredential == 0 ? "" : "_DevData.isCredential=" + pubdata.DevData.isCredential + "\r\n") +
                         "_DevData.EEPROM_size=0x" + pubdata.DevData.EEPROM_size.ToString("X2") + "\r\n" +
                         (skipStdValues && pubdata.DevData.configuration == 0 ? "" : "_DevData.configuration=" + pubdata.DevData.configuration + "\r\n") +
                         (skipStdValues && pubdata.DevData.last_VN_buffer_entries == 0 ? "" : "_DevData.last_VN_buffer_entries=" + pubdata.DevData.last_VN_buffer_entries + "\r\n") +
                         (skipStdValues && pubdata.DevData.Lv_EA_Command_Buffer_8 == 0 ? "" : "_DevData.Lv_EA_Command_Buffer_8=" + pubdata.DevData.Lv_EA_Command_Buffer_8 + "\r\n") +
                         (skipStdValues && pubdata.DevData.sequence_counter == 0 ? "" : "_DevData.sequence_counter=" + pubdata.DevData.sequence_counter + "\r\n") +
                         "_DevData.progCounter=" + pubdata.DevData.progCounter.ToString("#,0").Replace(',', '.') +
                            " 1:" + pubdata.DevData.progCounter_1.ToString("#,0").Replace(',', '.') +
                            " 2:" + pubdata.DevData.progCounter_2.ToString("#,0").Replace(',', '.') +
                            " 3:" + pubdata.DevData.progCounter_3.ToString("#,0").Replace(',', '.') +
                            " rd:" + pubdata.DevData.progCounter_rd.ToString("#,0").Replace(',', '.') +
                            " MAX:" + pubdata.DevData.progCounter_Max.ToString("#,0").Replace(',', '.') + "\r\n" +
                         (skipStdValues && pubdata.DevData.last_accesslist_entries == 0 ? "" : "_DevData.last_accesslist_entries=" + pubdata.DevData.last_accesslist_entries + "\r\n") +
                         (skipStdValues && pubdata.DevData.Lv_Profile_Change_Requests == 0 ? "" : "_DevData.Lv_Profile_Change_Requests=" + pubdata.DevData.Lv_Profile_Change_Requests + "\r\n") +
                         (skipStdValues && pubdata.DevData.Lv_VN_downstream_data == 0 ? "" : "_DevData.Lv_VN_downstream_data=" + pubdata.DevData.Lv_VN_downstream_data + "\r\n") +
                         (skipStdValues && pubdata.DevData.PHI_ext == 0 ? "" : "_DevData.PHI_ext=" + pubdata.DevData.PHI_ext + "\r\n") +
                         (skipStdValues && pubdata.DevData.devType == 1 ? "" : "_DevData.devType=" + pubdata.DevData.devType + "\r\n") +
                         (skipStdValues && pubdata.DevData.subDeviceClass == 0 ? "" : "_DevData.subDeviceClass=" + pubdata.DevData.subDeviceClass + "\r\n") +
                         (skipStdValues && pubdata.DevData.PHIRange == 1 ? "" : "_DevData.PHIRange=" + pubdata.DevData.PHIRange + "\r\n") +
                         (skipStdValues && pubdata.DevData.localLockCount == 1 ? "" : "_DevData.localLockCount=" + pubdata.DevData.localLockCount + "\r\n") +
                         "_DevData.deviceClass= Base:" + pubdata.DevData.deviceClassBase +
                                                 " Orig:" + pubdata.DevData.deviceClassOrig + "\r\n" +
                         "_DevData.appClass=    Base:" + pubdata.DevData.appClassBase +
                                                 " Orig:" + pubdata.DevData.appClassOrig + "\r\n" +
                         "_DevData.deviceIdentifier=0x" + pubdata.DevData.deviceIdentifier.ToString("X16") + "\r\n" +
                         "_DevData.FW_version= SE:" + pubdata.DevData.FW_version_SE +
                                               " SSC:" + pubdata.DevData.FW_version_SSC + (pubdata.DevData.FW_version_SSC_isBootloader ? " (SSC-BL)" : "") +
                                        " Bootloader:" + pubdata.DevData.FW_version_Bootloader + "\r\n" +
                         "_DevData.logicalInputStates=0x" + logicalInputStates.ToString("X4") + ":" + pubdata.DevData.logicalInputStatesDecoded + "    (_DevData.logicalInputs=" + pubdata.DevData.DecodeLogicalInputs() + ")\r\n" +
                         (skipStdValues && pubdata.DevData.SD_Version == 0 ? "" : "_DevData.SD_Version=0x" + pubdata.DevData.SD_Version.ToString("X2") + "\r\n") +
                         (skipStdValues && pubdata.DevData.client_IP_Addresses[0] == 0
                                    && pubdata.DevData.client_IP_Addresses[1] == 0
                                    && pubdata.DevData.client_IP_Addresses[2] == 0 ? "" : "_DevData.client_IP_Addresses=" + ArrayToString(pubdata.DevData.client_IP_Addresses, "X2\t0x") + "\r\n");
                        // Hide table for SR30-Master
                        if (skipStdValues == false || pubdata.ProductName != "SR30GWM_SV")
                        {
                            result +=
                             "_DevData.installedAppFW=       " + ArrayToString(pubdata.DevData.installedAppFW, "u16ver", 5) + "\r\n" +
                             "_DevData_APP-Table-Idx(fromSE) [_0_] [_1_] [_2_] [_3_] [_4_] [_5_] [_6_] [_7_]\r\n" +
                             "_DevData.installedApps=        " + ArrayToString(pubdata.DevData.installedApps, "d", 5) + "\r\n" +
                             "_DevData.installedAppClass=    " + ArrayToString(pubdata.DevData.installedAppClass, "d", 5) + "\r\n" +
                             "_DevData.installedAppClassBase=" + ArrayToString(pubdata.DevData.installedAppClassBase, "d", 5) + "\r\n" +
                             "_DevData.installedAppVariantID=" + ArrayToString(pubdata.DevData.installedAppVariantID, "d", 5) + "\r\n" +
                             "_DevData.FormatDbVersion=      " + ArrayToString(pubdata.DevData.FormatDbVersion, "u16ver", 5) + "\r\n";
                        }
                    }
                    if (pubdata.ROMVariables != null)
                    {
                        result += "   ROMVariables=" + pubdata.ROMVariables + "\r\n";
                    }
                    if (pubdata.Counters != null)
                    {
                        result += "   Counters=" + pubdata.Counters + "\r\n";
                    }

                    if (pubdata.Next != null)
                    {
                        result += "\r\n" +
                            $"Slave ({pubdata.Next.ProductName}:{pubdata.Next.PHI}@{pubdata.Next.CommPathSlaveIdx}):\r\n";
                    }
                    pubdata = pubdata.Next;
                }
                return result;
            }
        }
        private string ArrayToString(object array, string format_PreString)
        {
            return ArrayToString(array, format_PreString, 0);
        }
        private string ArrayToString(object array, string format_PreString, int length)
        {
            string str = "";
            string[] format_PreStringArray = format_PreString.Split('\t');
            string fmt = format_PreStringArray[0];
            string preString = (format_PreStringArray.Length >= 2 ? format_PreStringArray[1] : "");

            if (array is uint[])
            {
                foreach (uint b in (uint[])array) { str += (preString + b.ToString(fmt)).PadLeft(length) + " "; }
            }
            else if (array is ushort[])
            {
                if (format_PreString == "u16ver") { foreach (ushort v in (ushort[])array) { str += (preString + (v / 0x100) + "." + (v % 0x100)).PadLeft(length) + " "; } }
                else { foreach (ushort v in (ushort[])array) { str += (preString + v.ToString(fmt)).PadLeft(length) + " "; } }
            }
            else if (array is byte[])
            {
                foreach (byte v in (byte[])array) { str += (preString + v.ToString(fmt)).PadLeft(length) + " "; }
            }
            else
            {
                str += array.ToString().PadLeft(length);
            }
            return str.TrimEnd(' ');
        }
        private string GetTitleForDVI(int dvi)
        {
            string resultTitle = "";
            try
            {
                driverResultEnum result;
                result = Device.DebugCmd(null, null, null,
                         "-2 '\t' " +
                         "select DeviceClasses.DevClassDefine,DeviceClasses.DevClassName,DevClassVariants.Titel,DevClassVariants.Remarks " +
                         "from DevClassVariants " +
                         "join DeviceClasses on DevClassID=DeviceClasses.ID " +
                         "where DevClassVariants.ID=" + dvi,
                         out string resultSQL);
                string[] lines = resultSQL.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                if (lines[1] == "[DevClassDefine]\t[DevClassName]\t[Titel]\t[Remarks]" &&
                    lines.Length > 3)
                {
                    resultTitle = $" -> ({lines[3].Replace("\t", " - ")})";
                }
            }
            finally
            {
                Device.CloseCom();
            }

            return resultTitle;
        }
        private string CheckDevData(string name, string ddString, string rpString)
        {
            if (rpString != ddString)
            {
                return " (DevData." + name + ":" + ddString + ")";
            }
            return "";
        }

        #endregion


    }
}
