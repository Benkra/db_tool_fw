﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace DB_Tool_FW
{
    internal static class DevService
    {
        /// <summary>
        /// Separates a string and writes every line into a List.
        /// </summary>
        /// <param name="data"></param>
        public static List<string> linesToList(string data)
        {
            List<string> listData = new List<string>();

            using (StringReader reader = new StringReader(data))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim();
                    listData.Add(line);
                    //Debug.WriteLine(line);
                }
            }

            return listData;
        }


        /// <summary>
        /// Writes the pubData String to a text file
        /// </summary>
        /// <param name="docName"></param>
        public static void writePubDataToTxt(string docName, string docPath, string sData)
        {

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, docName)))
            {
                outputFile.Write(sData);

            }

        }


        /// <summary>
        /// Reads the Data from a txt file and writes it in to the sPubdata String
        /// </summary>
        /// <param name="docName"></param>
        public static List<string> readPubDataFromTxt(string docName, string docPath)
        {
            string sData;
            List<string> listData = new List<string>();

            try
            {
                StreamReader reader = new StreamReader(Path.Combine(docPath, docName));
                sData = reader.ReadToEnd();
                listData = linesToList(sData);
                return listData;

            }
            catch (Exception)
            {

                throw;
            }
        }



        /// <summary>
        /// Returns a List with all m_Attributes in the m_ListPubvData list
        /// </summary>
        /// <returns></returns>
        public static List<string> AttributesToList(List<string> rawData)
        {
            var attributes = new List<string>();
            int start, end = 0, counter = 0;
            char myChar = '=';


            foreach (var item in rawData)
            {


                end = item.IndexOf("=");

                if (end == -1)
                {
                    continue;
                }

                while (true)
                {

                    myChar = char.Parse(item.Substring(end - counter, 1));

                    if (myChar.Equals(' ') || myChar.Equals('\n') || counter == end || myChar.Equals('('))
                    {
                        start = end - counter + 1;

                        attributes.Add(item.Substring(start - 1, counter));

                        counter = 1;
                        start = 0;
                        end = 0;
                        break;
                    }

                    ++counter;

                }

            }

            //foreach (var data in attributes)
            //{
            //    Console.WriteLine(data);
            //}
            return attributes;

        }


        /// <summary>
        /// Returns a dictionary with the atribute Name as key and the value as value
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dataName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static Dictionary<string, string> AtributeValuePairsToDic(List<string> atributes, List<string> rawDatas)
        {
            Dictionary<string, string> atributesValuePairs = new Dictionary<string, string>();
            string sTempString = null;
            int start, index;

            //foreach (var item in rawDatas)
            //{
            //    Debug.WriteLine(item);
            //}

            //Debug.WriteLine("Atribute::::");
            //foreach (var item in atributes)
            //{
            //    Debug.WriteLine(item);
            //}
            //Debug.WriteLine("Atribute Ende");


            for (int i = 0; i<atributes.Count; i++)
            {

                for(int j = 0; i < rawDatas.Count; j++)
                {
                    //Debug.WriteLine(data);

                    if (rawDatas[j].Contains(atributes[i]))
                    {
                        start = rawDatas[j].IndexOf("=");
                        sTempString = rawDatas[j].Substring(start + 1);
                        sTempString = sTempString.Trim();
                        

                        if (atributesValuePairs.ContainsKey(atributes[i]))
                        {
                            atributes[i] = atributes[i] + "_2";
                        }
                        atributesValuePairs.Add(atributes[i], sTempString);

                        //Debug.WriteLine("AtributVauluePair: " + atributes[i] + " == " + sTempString);


                        break;


                    }
                    else
                    {
                        continue;
                    }

                }
                
            }
            if (sTempString.Length == 0)
            {
                throw new Exception("Beim Auslesen der Daten aus dem Datenstring ist ein Problem aufgetreten");
            }

            return atributesValuePairs;
        }


    }
}