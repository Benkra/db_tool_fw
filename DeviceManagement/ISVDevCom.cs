﻿using InitG2ToolDrv;
using System.Collections.Generic;

namespace DB_Tool_FW
{
    public interface ISVDevCom
    {
        string sCommunicationPath { get; set; }

        DataModel CreateAndGetDataModel(int prozessId, int readNumberator);
        void GetSmartCDFW();
        DataModel ReadPublicData(int prozessId, int readNumberator);
    }
}