﻿using InitG2ToolDrv;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Windows.Forms;
using DB_Tool_FW.GUI.View;
using System.Diagnostics;
using DB_Tool_FW.DB_and_Data;
using DB_Tool_FW.GUI.Presenter;


namespace DB_Tool_FW
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Test.RunTest();

            DB_Manager dB_Manager = DB_Manager.Instanz;
            SVDevCom sVDevCom = new SVDevCom();
            DataService dataService = new DataService(dB_Manager, sVDevCom);
            MainView mainView = new MainView();

            Presenter presenter = new Presenter(mainView, dataService);

            Application.Run(mainView);


        }
    }
}
