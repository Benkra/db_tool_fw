﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DB_Tool_FW.DB_and_Data;
using DB_Tool_FW;
using DB_Tool_FW.GUI.View;
using FluentAssertions;
using System.CodeDom;
using System.Threading;
using Microsoft.Win32;
using System.Media;
using static System.Net.Mime.MediaTypeNames;
using System.Data.Entity.Core.Common;

namespace DB_Tool_FW.GUI.Presenter
{
    public class Presenter
    {
        //Fields
        private MainView view;
        private IDataService dataService;
        private BindingSource saveBindingSource, searchBindingSource;
        private BindingSource devDataDesignationsBindigSource;
        private BindingSource comarisonSymbolsBindingSource;

        //private ProzessModel prozessModel;




        public Presenter(MainView view, IDataService dataService)
        {
            this.view = view;
            this.dataService = dataService;

            this.saveBindingSource = new BindingSource();
            this.devDataDesignationsBindigSource = new BindingSource();
            this.comarisonSymbolsBindingSource = new BindingSource();

            //Subscribe event halder methods to view events
            this.view.OnNewProzessClicked += View_OnNewProzessClicked;
            view.OnTbMainChanged += View_OnTbMainChanged;
            view.OnReadPublicClicked += View_OnReadPublicClicked;
            view.OnSaveClicked += View_OnSaveClicked;
            view.OnBtnSearchProzessClicked += View_OnSearchProzessClicked;
            view.OnBtnEditProzessClicked += View_OnEditProzsessClicked;
            view.OnRemoveBtnClicked += View_OnRemoveBtnClicked;
            view.OnDevDatasSelectedChanged += View_OnDevDatasSelectedChanged;
            view.OnBtnChoseDbClicked += View_OnBtnChoseDbClicked;
            view.OnBtnCreatDbClicked += View_OnBtnCreateDbClicked;
            view.OnBtnSearchDevData += View_OnBtnSearchDevDataClicked;
            view.OnTbDevDataSearchChanged += View_OnTbDevDataSearchChanged;
            //Subscribe event halder methods to dataService events
            ///////////////////////////////////////////////////////dataService.OnProzessInstanceChanged += View_OnProzessInstanceChanged;
            //Set binding sources
            view.SetSaveSelectBindingSource(saveBindingSource);
            view.SetDevDataDesignationsBindingSource(devDataDesignationsBindigSource);
            view.SetComparisonSymbolsBindingSource(comarisonSymbolsBindingSource);
            //view.SetSearchForBindingSource(searchBindingSource);
            //LoadBindingSources
            LoadBindingSources();
           

            view.SetTbCurrentDb(dataService.GetDbConnection());

        }

        

        private void EnalbleCompnentsProzessChanged()
        {
            if (dataService.ProzessModel != null)
            {
                view.EnableReadPublicBtn(true);
            }
            else
            {
                view.EnableReadPublicBtn(false);
            }

            if (dataService.ProzessModel != null)
            {
                view.SetTbCurrentProzess((int)dataService.ProzessModel.ProzessId.ItemValue, (string)dataService.ProzessModel.RkNumber.ItemValue,
                    (string)dataService.ProzessModel.Capturer.ItemValue,
                    (string)dataService.ProzessModel.ReceiptDateRnD.ItemValue);

            }

        }

        public void LoadBindingSources()
        {
            dataService.ArrangeDesignations();

            saveBindingSource.DataSource = dataService.DevDatas;
            devDataDesignationsBindigSource.DataSource = dataService.DevDataDesignations;
            comarisonSymbolsBindingSource.DataSource= dataService.CompariaonSymbols;

           

            //searchBindingSource.DataSource = dataService.DevDatas;

        }



        #region Tab "Daten Eingabe"

        private void View_OnNewProzessClicked(object sender, EventArgs e)
        {
            if (dataService.GetDbConnection() == string.Empty)
            {
                MessageBox.Show("Es besteht keine gülige Verbindung zu einem Server!");
                return;

            }
            using (NewProzessDialog newProzessDialog = new NewProzessDialog())
            {

                if (newProzessDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    dataService.CreateNewProzess();
                    

                    dataService.ProzessModel.Capturer.ItemValue = newProzessDialog.Capturer;
                    dataService.ProzessModel.RkNumber.ItemValue = newProzessDialog.RKNumber;
                    dataService.ProzessModel.PHI.ItemValue = newProzessDialog.Phi;
                    dataService.ProzessModel.ContactPersonSV.ItemValue = newProzessDialog.ContactSv;
                    dataService.ProzessModel.Trader.ItemValue = newProzessDialog.Trader;
                    dataService.ProzessModel.ReceiptDateRnD.ItemValue = newProzessDialog.ReceiptDateRnD;
                    dataService.ProzessModel.Door.ItemValue = newProzessDialog.Door;
                    dataService.ProzessModel.History.ItemValue = newProzessDialog.History;
                    dataService.ProzessModel.ErrorDescription.ItemValue = newProzessDialog.ErrorDescription;
                    dataService.ProzessModel.EndCustomer.ItemValue = newProzessDialog.EndCustomer;
                    dataService.ProzessModel.ItemNumber.ItemValue = newProzessDialog.ItemNumber;




                    //dataService.ChangeProzessModel(prozessModel);

                    EnalbleCompnentsProzessChanged();

                    dataService.ProzessSaved = false;


                    view.ShowProzess(dataService.ProzessModel);
                    view.EnableSaveBtn(true);
                    view.EnableEditProzessBtn(true);


                }

            }

        }

        private void View_OnEditProzsessClicked(object sender, EventArgs e)
        {
            using (NewProzessDialog newProzessDialog = new NewProzessDialog())
            {

                if (dataService.ProzessModel != null)
                {
                    ProzessModel pm = dataService.ProzessModel;
                    newProzessDialog.SetText(pm.Capturer.ItemValue.ToString(), pm.RkNumber.ItemValue.ToString(),
                        pm.PHI.ItemValue.ToString(), pm.ItemNumber.ItemValue.ToString(), pm.ContactPersonSV.ItemValue.ToString(),
                        pm.Trader.ItemValue.ToString(), pm.EndCustomer.ItemValue.ToString(), pm.Door.ItemValue.ToString(),
                        pm.ReceiptDateRnD.ItemValue.ToString());

                }

                if (newProzessDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                  


                    dataService.ProzessModel.Capturer.ItemValue = newProzessDialog.Capturer;
                    dataService.ProzessModel.RkNumber.ItemValue = newProzessDialog.RKNumber;
                    dataService.ProzessModel.PHI.ItemValue = newProzessDialog.Phi;
                    dataService.ProzessModel.ContactPersonSV.ItemValue = newProzessDialog.ContactSv;
                    dataService.ProzessModel.Trader.ItemValue = newProzessDialog.Trader;
                    dataService.ProzessModel.ReceiptDateRnD.ItemValue = newProzessDialog.ReceiptDateRnD;
                    dataService.ProzessModel.Door.ItemValue = newProzessDialog.Door;
                    dataService.ProzessModel.History.ItemValue = newProzessDialog.History;
                    dataService.ProzessModel.ErrorDescription.ItemValue = newProzessDialog.ErrorDescription;
                    dataService.ProzessModel.EndCustomer.ItemValue = newProzessDialog.EndCustomer;
                    dataService.ProzessModel.ItemNumber.ItemValue = newProzessDialog.ItemNumber;




                    EnalbleCompnentsProzessChanged();
                    dataService.ProzessSaved = false;


                    view.ShowProzess(dataService.ProzessModel);
                    view.EnableSaveBtn(true);


                }

            }

        }

        private void View_OnCbReadChanged(object sender, EventArgs e)
        {
            MainView test = (MainView)sender;

        }

        private void View_OnTbMainChanged(object sender, EventArgs e)
        {
            if (view.mainTbIsText)
            {
                view.EnableClearBtn(true);

            }
            else
            {
                view.EnableClearBtn(false);
            }

        }

        private void View_OnReadPublicClicked(object sender, EventArgs e)
        {
            try
            {
                dataService.ReadPublicDataFromDevice(dataService.ProzessModel.intPorozessId);
            }
            catch (Exception ex)
            {
                view.ShowError("Beim Auslesen des Gerätes ist ein Fehler aufetreten... " + '\n' + '\r' + ex.Message);
                
            }


            if (dataService.CurrentDataModel == null)
            {
                MessageBox.Show("Kein Gerät erkannt!");
                return;
            }       
            view.ShowDevData(dataService.CurrentDataModel);

            view.EnableRemoveBtn(true);
            view.EnableSaveBtn(true);
            dataService.DevDataSaved = false;

            /////////////////////////////////////////////////////// tbd
            /// Check if the phi of the read device is equal to tht phi of the las read device

        }

        private void View_OnSaveClicked(object sender, EventArgs e)
        {
            if (dataService.ProzessModel != null && dataService.ProzessSaved == false)
            {

                try
                {
                    dataService.WriteProzessToDb();
                }
                catch (Exception ex)
                {

                    view.ShowError("Ein Fehler beim Versuch die Prozessdaten auf die Datenbank" +
                        "zu schreiben ist ein Fehler aufgetreten: "
                        + '\r' + '\n' + ex.ToString());
                }


                dataService.ProzessSaved = true;
                view.ShowSaved("Vorgang Nr.: " + dataService.ProzessModel.ProzessId.ItemValue);
                view.EnableSaveBtn(false);
                view.EnableEditProzessBtn(false);


            }
            if (dataService.CurrentDataModel != null && dataService.DevDataSaved == false
                && (int)dataService.ProzessModel.ProzessId.ItemValue == (int)dataService.CurrentDataModel.ProzessId)
            {
                try
                {
                    dataService.WriteDevDataToDb();
                }
                catch (Exception ex)
                {

                    view.ShowError("Ein Fehler beim Versuch die Gerätedaten auf die Datenbank" +
                        "zu schreiben ist ein Fehler aufgetreten: "
                        + '\r' + '\n' + ex.ToString());
                }


                dataService.DevDataSaved = true;
                view.EnableSaveBtn(false);
                view.EnableRemoveBtn(false);
                view.ShowSaved("Vorgang Nr.: " + 
                   dataService.GetValue("PHI", (int)dataService.CurrentDataModel.ProzessId, dataService.CurrentDataModel.MaxNumberator  ));


            }

        }

        private void View_OnRemoveBtnClicked(object sender, EventArgs e)
        {

            if (dataService.DevDatas.Count > 0)
            {
                dataService.DevDatas.RemoveAt(dataService.DevDatas.Count - 1);
            }

            if(dataService.DevDatas.Count == 0)
            {
                view.EnableRemoveBtn(false);
            }

        }  

        #endregion





        #region Tab "Daten Anzeigen"
        private void View_OnSearchProzessClicked(object sender, EventArgs e)
        {
            string searchValue = view.searchTxt;
            List<ProzessModel> prozessModels = new List<ProzessModel>();
            string text = "";

            if (searchValue != String.Empty)
            {
                try
                {
                    prozessModels = dataService.FindProzesses(searchValue);
                }
                catch (Exception ex)
                {
                    view.SetTextTbShowData("Da ist etwas schief gegangen...." +
                        '\n' + ex);
                }

                foreach (var item in prozessModels)
                {
                    text = text + item.ToStringInline() + '\r' + '\n';

                }
                view.SetTextTbShowData(text);

            }

        }

        private void View_OnDevDatasSelectedChanged(object sender, EventArgs e)
        {
            if (dataService.DevDatas.Count != 0)
            {
                view.ShowDevData(dataService.DevDatas.ElementAt(view.indexLB_selectedItem));
            }


        }

        private void View_OnBtnSearchDevDataClicked(object sender, EventArgs e)
        {
            List<DataModel> dataModels = new List<DataModel>();
            string text = string.Empty;
            if (view.searchValueTxt != string.Empty)
            {
                try
                {
                    dataModels = dataService.FindDevData(view.comparisonSymbol, view.searchDevDataDesignation, view.searchValueTxt);

                }
                catch (Exception ex)
                {

                    view.SetTextTbShowData("Da ist etwas schief gegangen...." +
                        '\n' + ex);
                }

                foreach (var item in dataModels)
                {
                    text = text + item.GetDataStringInline() + '\r' + '\n' + '\r' + '\n';

                }
                if (text == string.Empty)
                {
                    text = "Keine Übereinstimmung gefunden!";

                }
                view.SetTextTbShowData(text);
            }

        }

        private void View_OnTbDevDataSearchChanged(object sender, EventArgs e)
        {
            if (view.searchValueTxt != string.Empty)
            {
                view.EnableBtnSearchDevData(true);

            }
            else
            {
                view.EnableBtnSearchDevData(false);
            }

        }
        #endregion





        #region Tab "Datenbank"
        private void View_OnBtnChoseDbClicked(object sender, EventArgs e)
        {
            string path = string.Empty;

            Thread t = new Thread((ThreadStart)(() => {
                System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

                openFileDialog.Filter = "all|*.*|sqlite3|*.sqlite3 |sqlite|*.sqlite | db3|*.db3 |db|*.db";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = openFileDialog.FileName;
                }
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

            if (path != string.Empty)
            {
                dataService.SetDbPath(path);

            }

            view.SetTbCurrentDb(dataService.GetDbConnection());

        }

        private void View_OnBtnCreateDbClicked(object sender, EventArgs e)
        {
            string path = string.Empty;


            Thread t = new Thread((ThreadStart)(() => {
                System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog();

                saveFileDialog.Filter = "all|*.*|sqlite3|*.sqlite3 |sqlite|*.sqlite | db3|*.db3 |db|*.db";
                saveFileDialog.FilterIndex = 5;
                saveFileDialog.RestoreDirectory = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    path = saveFileDialog.FileName;
                }
            }));

            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();

            if (path != string.Empty)
            {
                dataService.NewDb(path);

            }

            view.SetTbCurrentDb(dataService.GetDbConnection());

        }

        #endregion




    }
}
