﻿namespace DB_Tool_FW.GUI.View
{
    partial class NewProzessDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the ProzessPropertys of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_contactSv = new System.Windows.Forms.TextBox();
            this.tb_Eingangsdatum_TXT = new System.Windows.Forms.TextBox();
            this.tb_door = new System.Windows.Forms.TextBox();
            this.tb_Tuer_TXT = new System.Windows.Forms.TextBox();
            this.tb_endCustomer = new System.Windows.Forms.TextBox();
            this.tb_EKunde_TXT = new System.Windows.Forms.TextBox();
            this.tb_trader = new System.Windows.Forms.TextBox();
            this.tb_Haendler_TXT = new System.Windows.Forms.TextBox();
            this.tb_receiptDateRnD = new System.Windows.Forms.TextBox();
            this.tb_KontaktSV_TXT = new System.Windows.Forms.TextBox();
            this.tb_itmeNumber = new System.Windows.Forms.TextBox();
            this.tb_Artikelnummer_TXT = new System.Windows.Forms.TextBox();
            this.tb_phi = new System.Windows.Forms.TextBox();
            this.tb_PHI_TXT = new System.Windows.Forms.TextBox();
            this.tb_rkNumber = new System.Windows.Forms.TextBox();
            this.tb_RK_Nummer_TXT = new System.Windows.Forms.TextBox();
            this.tb_capturer = new System.Windows.Forms.TextBox();
            this.tb_Erfasser_TXT = new System.Windows.Forms.TextBox();
            this.bt_addHistory = new System.Windows.Forms.Button();
            this.bt_addDescription = new System.Windows.Forms.Button();
            this.bt_addProzess = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_contactSv);
            this.groupBox1.Controls.Add(this.tb_Eingangsdatum_TXT);
            this.groupBox1.Controls.Add(this.tb_door);
            this.groupBox1.Controls.Add(this.tb_Tuer_TXT);
            this.groupBox1.Controls.Add(this.tb_endCustomer);
            this.groupBox1.Controls.Add(this.tb_EKunde_TXT);
            this.groupBox1.Controls.Add(this.tb_trader);
            this.groupBox1.Controls.Add(this.tb_Haendler_TXT);
            this.groupBox1.Controls.Add(this.tb_receiptDateRnD);
            this.groupBox1.Controls.Add(this.tb_KontaktSV_TXT);
            this.groupBox1.Controls.Add(this.tb_itmeNumber);
            this.groupBox1.Controls.Add(this.tb_Artikelnummer_TXT);
            this.groupBox1.Controls.Add(this.tb_phi);
            this.groupBox1.Controls.Add(this.tb_PHI_TXT);
            this.groupBox1.Controls.Add(this.tb_rkNumber);
            this.groupBox1.Controls.Add(this.tb_RK_Nummer_TXT);
            this.groupBox1.Controls.Add(this.tb_capturer);
            this.groupBox1.Controls.Add(this.tb_Erfasser_TXT);
            this.groupBox1.Location = new System.Drawing.Point(18, 3);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(942, 631);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // tb_contactSv
            // 
            this.tb_contactSv.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_contactSv.Location = new System.Drawing.Point(507, 286);
            this.tb_contactSv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_contactSv.Name = "tb_contactSv";
            this.tb_contactSv.Size = new System.Drawing.Size(396, 30);
            this.tb_contactSv.TabIndex = 17;
            // 
            // tb_Eingangsdatum_TXT
            // 
            this.tb_Eingangsdatum_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_Eingangsdatum_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Eingangsdatum_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Eingangsdatum_TXT.Location = new System.Drawing.Point(50, 522);
            this.tb_Eingangsdatum_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_Eingangsdatum_TXT.Name = "tb_Eingangsdatum_TXT";
            this.tb_Eingangsdatum_TXT.ReadOnly = true;
            this.tb_Eingangsdatum_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_Eingangsdatum_TXT.TabIndex = 16;
            this.tb_Eingangsdatum_TXT.Text = "Eingangsdatum";
            // 
            // tb_door
            // 
            this.tb_door.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_door.Location = new System.Drawing.Point(507, 462);
            this.tb_door.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_door.Name = "tb_door";
            this.tb_door.Size = new System.Drawing.Size(396, 30);
            this.tb_door.TabIndex = 15;
            // 
            // tb_Tuer_TXT
            // 
            this.tb_Tuer_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_Tuer_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Tuer_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Tuer_TXT.Location = new System.Drawing.Point(50, 472);
            this.tb_Tuer_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_Tuer_TXT.Name = "tb_Tuer_TXT";
            this.tb_Tuer_TXT.ReadOnly = true;
            this.tb_Tuer_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_Tuer_TXT.TabIndex = 14;
            this.tb_Tuer_TXT.Text = "Tür";
            // 
            // tb_endCustomer
            // 
            this.tb_endCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_endCustomer.Location = new System.Drawing.Point(507, 402);
            this.tb_endCustomer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_endCustomer.Name = "tb_endCustomer";
            this.tb_endCustomer.Size = new System.Drawing.Size(396, 30);
            this.tb_endCustomer.TabIndex = 13;
            // 
            // tb_EKunde_TXT
            // 
            this.tb_EKunde_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_EKunde_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_EKunde_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_EKunde_TXT.Location = new System.Drawing.Point(50, 412);
            this.tb_EKunde_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_EKunde_TXT.Name = "tb_EKunde_TXT";
            this.tb_EKunde_TXT.ReadOnly = true;
            this.tb_EKunde_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_EKunde_TXT.TabIndex = 12;
            this.tb_EKunde_TXT.Text = "Endkunde";
            // 
            // tb_trader
            // 
            this.tb_trader.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_trader.Location = new System.Drawing.Point(507, 346);
            this.tb_trader.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_trader.Name = "tb_trader";
            this.tb_trader.Size = new System.Drawing.Size(396, 30);
            this.tb_trader.TabIndex = 11;
            // 
            // tb_Haendler_TXT
            // 
            this.tb_Haendler_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_Haendler_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Haendler_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Haendler_TXT.Location = new System.Drawing.Point(50, 351);
            this.tb_Haendler_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_Haendler_TXT.Name = "tb_Haendler_TXT";
            this.tb_Haendler_TXT.ReadOnly = true;
            this.tb_Haendler_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_Haendler_TXT.TabIndex = 10;
            this.tb_Haendler_TXT.Text = "Händler";
            // 
            // tb_receiptDateRnD
            // 
            this.tb_receiptDateRnD.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_receiptDateRnD.Location = new System.Drawing.Point(507, 517);
            this.tb_receiptDateRnD.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_receiptDateRnD.Name = "tb_receiptDateRnD";
            this.tb_receiptDateRnD.Size = new System.Drawing.Size(396, 30);
            this.tb_receiptDateRnD.TabIndex = 9;
            // 
            // tb_KontaktSV_TXT
            // 
            this.tb_KontaktSV_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_KontaktSV_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_KontaktSV_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_KontaktSV_TXT.Location = new System.Drawing.Point(50, 291);
            this.tb_KontaktSV_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_KontaktSV_TXT.Name = "tb_KontaktSV_TXT";
            this.tb_KontaktSV_TXT.ReadOnly = true;
            this.tb_KontaktSV_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_KontaktSV_TXT.TabIndex = 8;
            this.tb_KontaktSV_TXT.Text = "Kontakt S&V";
            // 
            // tb_itmeNumber
            // 
            this.tb_itmeNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_itmeNumber.Location = new System.Drawing.Point(507, 229);
            this.tb_itmeNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_itmeNumber.Name = "tb_itmeNumber";
            this.tb_itmeNumber.Size = new System.Drawing.Size(396, 30);
            this.tb_itmeNumber.TabIndex = 7;
            // 
            // tb_Artikelnummer_TXT
            // 
            this.tb_Artikelnummer_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_Artikelnummer_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Artikelnummer_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Artikelnummer_TXT.Location = new System.Drawing.Point(50, 234);
            this.tb_Artikelnummer_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_Artikelnummer_TXT.Name = "tb_Artikelnummer_TXT";
            this.tb_Artikelnummer_TXT.ReadOnly = true;
            this.tb_Artikelnummer_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_Artikelnummer_TXT.TabIndex = 6;
            this.tb_Artikelnummer_TXT.Text = "Artikelnummer";
            // 
            // tb_phi
            // 
            this.tb_phi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_phi.Location = new System.Drawing.Point(507, 174);
            this.tb_phi.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_phi.Name = "tb_phi";
            this.tb_phi.Size = new System.Drawing.Size(396, 30);
            this.tb_phi.TabIndex = 5;
            // 
            // tb_PHI_TXT
            // 
            this.tb_PHI_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_PHI_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_PHI_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_PHI_TXT.Location = new System.Drawing.Point(50, 180);
            this.tb_PHI_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_PHI_TXT.Name = "tb_PHI_TXT";
            this.tb_PHI_TXT.ReadOnly = true;
            this.tb_PHI_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_PHI_TXT.TabIndex = 4;
            this.tb_PHI_TXT.Text = "PHI";
            // 
            // tb_rkNumber
            // 
            this.tb_rkNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_rkNumber.Location = new System.Drawing.Point(507, 118);
            this.tb_rkNumber.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_rkNumber.MaxLength = 10;
            this.tb_rkNumber.Name = "tb_rkNumber";
            this.tb_rkNumber.Size = new System.Drawing.Size(396, 30);
            this.tb_rkNumber.TabIndex = 3;
            // 
            // tb_RK_Nummer_TXT
            // 
            this.tb_RK_Nummer_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_RK_Nummer_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_RK_Nummer_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_RK_Nummer_TXT.Location = new System.Drawing.Point(50, 125);
            this.tb_RK_Nummer_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_RK_Nummer_TXT.Name = "tb_RK_Nummer_TXT";
            this.tb_RK_Nummer_TXT.ReadOnly = true;
            this.tb_RK_Nummer_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_RK_Nummer_TXT.TabIndex = 2;
            this.tb_RK_Nummer_TXT.Text = "RK_Nummer";
            // 
            // tb_capturer
            // 
            this.tb_capturer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_capturer.Location = new System.Drawing.Point(507, 63);
            this.tb_capturer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_capturer.Name = "tb_capturer";
            this.tb_capturer.Size = new System.Drawing.Size(396, 30);
            this.tb_capturer.TabIndex = 1;
            // 
            // tb_Erfasser_TXT
            // 
            this.tb_Erfasser_TXT.BackColor = System.Drawing.SystemColors.Menu;
            this.tb_Erfasser_TXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Erfasser_TXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_Erfasser_TXT.Location = new System.Drawing.Point(50, 69);
            this.tb_Erfasser_TXT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_Erfasser_TXT.Name = "tb_Erfasser_TXT";
            this.tb_Erfasser_TXT.ReadOnly = true;
            this.tb_Erfasser_TXT.Size = new System.Drawing.Size(388, 23);
            this.tb_Erfasser_TXT.TabIndex = 0;
            this.tb_Erfasser_TXT.Text = "Erfasser *";
            // 
            // bt_addHistory
            // 
            this.bt_addHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_addHistory.Location = new System.Drawing.Point(18, 669);
            this.bt_addHistory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bt_addHistory.Name = "bt_addHistory";
            this.bt_addHistory.Size = new System.Drawing.Size(388, 66);
            this.bt_addHistory.TabIndex = 9;
            this.bt_addHistory.Text = "Historie hinzufügen";
            this.bt_addHistory.UseVisualStyleBackColor = true;
            // 
            // bt_addDescription
            // 
            this.bt_addDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_addDescription.Location = new System.Drawing.Point(414, 668);
            this.bt_addDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bt_addDescription.Name = "bt_addDescription";
            this.bt_addDescription.Size = new System.Drawing.Size(386, 68);
            this.bt_addDescription.TabIndex = 10;
            this.bt_addDescription.Text = "Fehler Beschreibung hinzufüge";
            this.bt_addDescription.UseVisualStyleBackColor = true;
            // 
            // bt_addProzess
            // 
            this.bt_addProzess.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt_addProzess.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_addProzess.Location = new System.Drawing.Point(817, 644);
            this.bt_addProzess.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bt_addProzess.Name = "bt_addProzess";
            this.bt_addProzess.Size = new System.Drawing.Size(206, 128);
            this.bt_addProzess.TabIndex = 11;
            this.bt_addProzess.Text = "Übernehmen";
            this.bt_addProzess.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(68, 605);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(876, 19);
            this.textBox3.TabIndex = 12;
            this.textBox3.Text = "* Angabe erfoderlich";
            // 
            // btn_cancel
            // 
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cancel.Location = new System.Drawing.Point(1031, 646);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(206, 128);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Abbrechen";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // NewProzessDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1478, 788);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.bt_addProzess);
            this.Controls.Add(this.bt_addDescription);
            this.Controls.Add(this.bt_addHistory);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "NewProzessDialog";
            this.Text = "Neuer Prozess";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_capturer;
        private System.Windows.Forms.TextBox tb_Erfasser_TXT;
        private System.Windows.Forms.TextBox tb_receiptDateRnD;
        private System.Windows.Forms.TextBox tb_KontaktSV_TXT;
        private System.Windows.Forms.TextBox tb_itmeNumber;
        private System.Windows.Forms.TextBox tb_Artikelnummer_TXT;
        private System.Windows.Forms.TextBox tb_phi;
        private System.Windows.Forms.TextBox tb_PHI_TXT;
        private System.Windows.Forms.TextBox tb_rkNumber;
        private System.Windows.Forms.TextBox tb_RK_Nummer_TXT;
        private System.Windows.Forms.TextBox tb_endCustomer;
        private System.Windows.Forms.TextBox tb_EKunde_TXT;
        private System.Windows.Forms.TextBox tb_trader;
        private System.Windows.Forms.TextBox tb_Haendler_TXT;
        private System.Windows.Forms.TextBox tb_door;
        private System.Windows.Forms.TextBox tb_Tuer_TXT;
        private System.Windows.Forms.Button bt_addHistory;
        private System.Windows.Forms.Button bt_addDescription;
        private System.Windows.Forms.Button bt_addProzess;
        private System.Windows.Forms.TextBox tb_contactSv;
        private System.Windows.Forms.TextBox tb_Eingangsdatum_TXT;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btn_cancel;
    }
}