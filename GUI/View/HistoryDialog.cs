﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Tool_FW.GUI.View
{
    public partial class HistoryDialog : Form
    {
        private string history;

        public HistoryDialog()
        {
            InitializeComponent();
            AssociateAndRaisViewEvents();
        }
        public string History { get => history; set => history = value; }


        private void AssociateAndRaisViewEvents()
        {
            bt_ok.Click += (e, s) =>
            {

                History = tb_history.Text;
                if (tb_history.Text == null)
                {
                    History = String.Empty;
                }

            };
        }

        public void SetHistoryText(string text)
        {
            tb_history.Text = text;
        }
    }
}
