﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Tool_FW.GUI.View
{
    
    public partial class ErrorDescriptionDialog : Form
    {
        string errorDescription;
        public ErrorDescriptionDialog()
        {
            InitializeComponent();
            AssociateAndRaisViewEvents();
        }

        public string ErrorDescription { get => errorDescription; set => errorDescription = value; }


        private void AssociateAndRaisViewEvents()
        {
            bt_ok.Click += (e, s) =>
            {

                ErrorDescription = tb_ErrorDescripton.Text;
                if (tb_ErrorDescripton == null)
                {
                    ErrorDescription = String.Empty;
                }

            };
        }

        

        public void SetTextErrordescription(string text)
        {

            tb_ErrorDescripton.Text = text;
        }
       
    }
}
