﻿using DB_Tool_FW.DB_and_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Tool_FW.GUI.View
{
    public partial class NewProzessDialog : Form
    {
        string capturer, rKNumber, phi, itemNumber, contactSv, trader, endCustomer,
            door, receiptDateRnD;
        string errorDescription = string.Empty;
        string history = string.Empty;

            
        public NewProzessDialog()
        {
            InitializeComponent();
            AssociateAndRaisViewEvents();
            
        }

        public string Capturer { get => capturer; set => capturer = value; }
        public string RKNumber { get => rKNumber; set => rKNumber = value; }
        public string Phi { get => phi; set => phi = value; }
        public string ItemNumber { get => itemNumber; set => itemNumber = value; }
        public string ContactSv { get => contactSv; set => contactSv = value; }
        public string Trader { get => trader; set => trader = value; }
        public string EndCustomer { get => endCustomer; set => endCustomer = value; }
        public string Door { get => door; set => door = value; }
        public string ReceiptDateRnD { get => receiptDateRnD; set => receiptDateRnD = value; }
        public string History { get => history; set => history = value; }
        public string ErrorDescription { get => errorDescription; set => errorDescription = value; }


        private void AssociateAndRaisViewEvents()
        {
            bt_addProzess.Enabled = false;

            tb_capturer.TextChanged += (s, e) =>
            {
                if (tb_capturer.Text != string.Empty)
                {
                    bt_addProzess.Enabled = true;
                }
                else
                {
                    bt_addProzess.Enabled = false;
                }

            };

            bt_addProzess.Click += (s, e) =>
            {
                Capturer = tb_capturer.Text;
                RKNumber = tb_rkNumber.Text;
                Phi = tb_phi.Text;
                ItemNumber = tb_itmeNumber.Text;
                ContactSv = tb_contactSv.Text;
                Trader = tb_trader.Text;
                EndCustomer = tb_endCustomer.Text;
                ReceiptDateRnD = tb_receiptDateRnD.Text;
                Door = tb_door.Text;


            };

            bt_addDescription.Click += (s, e) =>
            {
                using (ErrorDescriptionDialog errorDescriptionDialog = new ErrorDescriptionDialog())
                {
                    errorDescriptionDialog.SetTextErrordescription(ErrorDescription);

                    if (errorDescriptionDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        
                        ErrorDescription = errorDescriptionDialog.ErrorDescription;
                                        
                    }

                }
            };

            bt_addHistory.Click += (s, e) =>
            {
                using (HistoryDialog historyDialog = new HistoryDialog())
                {
                    historyDialog.SetHistoryText(History);

                    if (historyDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        History = historyDialog.History;

                    }


                }
            };

        }

        public void SetText(string capturer, string rkNumber, string phi, string itemNumber, string contactSV,
            string trader, string endCustomer, string door, string receiptDateRnD)
        {
            tb_capturer.Text = capturer;
            tb_rkNumber.Text = rkNumber;
            tb_phi.Text = phi;
            tb_itmeNumber.Text = itemNumber;
            tb_contactSv.Text = contactSV;
            tb_trader.Text = trader;
            tb_endCustomer.Text = endCustomer;
            tb_door.Text = door;
            tb_receiptDateRnD.Text = receiptDateRnD;


        }
    }
}
