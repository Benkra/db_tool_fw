﻿namespace DB_Tool_FW.GUI.View
{
    partial class ErrorDescriptionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the ProzessPropertys of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_ErrorDescripton = new System.Windows.Forms.RichTextBox();
            this.bt_ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_ErrorDescripton
            // 
            this.tb_ErrorDescripton.Location = new System.Drawing.Point(44, 50);
            this.tb_ErrorDescripton.Name = "tb_ErrorDescripton";
            this.tb_ErrorDescripton.Size = new System.Drawing.Size(706, 279);
            this.tb_ErrorDescripton.TabIndex = 0;
            this.tb_ErrorDescripton.Text = "";
            // 
            // bt_ok
            // 
            this.bt_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bt_ok.Location = new System.Drawing.Point(294, 373);
            this.bt_ok.Name = "bt_ok";
            this.bt_ok.Size = new System.Drawing.Size(223, 42);
            this.bt_ok.TabIndex = 1;
            this.bt_ok.Text = "OK";
            this.bt_ok.UseVisualStyleBackColor = true;
            // 
            // ErrorDescriptionDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bt_ok);
            this.Controls.Add(this.tb_ErrorDescripton);
            this.Name = "ErrorDescriptionDialog";
            this.Text = "Fehlerbeschreibung";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox tb_ErrorDescripton;
        private System.Windows.Forms.Button bt_ok;
    }
}