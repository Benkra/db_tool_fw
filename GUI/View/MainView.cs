﻿using DB_Tool_FW.DB_and_Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace DB_Tool_FW.GUI.View
{
    public partial class MainView : Form, IView
    {

        //Fields
        private List<string> readColection;
        private List<string> dataCollection;
        private string selectedData;

        public string saveTxt;
        public string searchTxt;
        public string searchPropertyTxt;
        public string searchValueTxt;
        public string comparisonSymbol;
        public string searchDevDataDesignation;

        public bool mainTbIsText;
        public bool readCbSelected;
        public int indexLB_selectedItem;


        //Properties
        public List<string> ReadCollection
        {
            get { return readColection; }
            set { ReadCollection = value; }
        }
        public List<string> DataCollection
        {
            get { return dataCollection; }
            set { dataCollection = value; }
        }

        public string SelectedData
        {
            get { return selectedData; }
            set { selectedData = value; }
        }

        public string SerachFor { get; set; }
      

        //Konstructor
        public MainView()
        {
            InitializeComponent();
            AssociateAndRaisViewEvents();

        }

        //EventHanlder
        public event EventHandler OnNewProzessClicked;
        public event EventHandler OnReadPublicClicked;
        public event EventHandler OnSaveClicked;
        public event EventHandler OnClearClicked;
        public event EventHandler OnAdditionalDataClicked;
        public event EventHandler OnRecordReturnes;
        public event EventHandler OnCbReadChanged;
        public event EventHandler OnTbMainChanged;
        public event EventHandler OnBtnSearchProzessClicked;
        public event EventHandler OnBtnEditProzessClicked;
        public event EventHandler OnRemoveBtnClicked;
        public event EventHandler OnDevDatasSelectedChanged;
        public event EventHandler OnBtnChoseDbClicked;
        public event EventHandler OnBtnCreatDbClicked;
        public event EventHandler OnBtnSearchDevData;
        public event EventHandler OnTbDevDataSearchChanged;
 



        private void AssociateAndRaisViewEvents()
        {

            btn_readPublic.Click += (s, e) =>
            {
                OnReadPublicClicked?.Invoke(this, e);
            };

            btn_clear.Click += (s, e) =>
            {
                tb_main.Clear();
            };

            btn_newProzess.Click += (s, e) =>
            {
                OnNewProzessClicked?.Invoke(this, e);
            };

            tb_main.TextChanged += (s, e) =>
            {
                if (tb_main.Text != string.Empty)
                {
                    mainTbIsText = true;

                }
                else
                {
                    mainTbIsText = false;
                }

                OnTbMainChanged?.Invoke(this, e);

            };

            btn_save.Click += (s, e) =>
            {                
                OnSaveClicked?.Invoke(this, e);
            };

            btn_prozessSearch.Click += (s, e) =>
            {
                OnBtnSearchProzessClicked?.Invoke(this, e);
            };

            btn_editProzess.Click += (s, e) =>
            {
                OnBtnEditProzessClicked?.Invoke(this, e);
            };

            lb_DevItems.Click += (s, e) =>
            {
                //Cb_ItemChanged();
                indexLB_selectedItem = lb_DevItems.SelectedIndex;
                OnDevDatasSelectedChanged?.Invoke(this, e);
            };

            btn_remove.Click += (s, e) =>
            {
                OnRemoveBtnClicked?.Invoke(this, e);
            };

            btn_choseDb.Click += (s, e) =>
            {
               
                OnBtnChoseDbClicked?.Invoke(this, e);
            };

            btn_createDB.Click += (s, e) =>
            {

                OnBtnCreatDbClicked?.Invoke(this, e);
            };

            btn_searchDevData.Click += (s, e) =>
            {

                OnBtnSearchDevData?.Invoke(this, e);

                

            };

            tb_DevDataSearchValue.TextChanged += (s, e) =>
            {
                searchValueTxt = tb_DevDataSearchValue.Text;
                OnTbDevDataSearchChanged?.Invoke(this, e);

               
                searchDevDataDesignation = cb_devDataFilter.SelectedItem.ToString();
                comparisonSymbol = cb_comparisonSymbols.SelectedItem.ToString();

            };


        }

        private void MainView_Load(object sender, EventArgs e)
        {
            EnableSaveBtn(false);
            EnableClearBtn(false);
            EnableReadBtn(false);
            EnableRemoveBtn(false);
            EnableBtnSearchDevData(false);
        }


        #region Tab "Daten Engabe"


        //Methodes
        public void ShowProzess(ProzessModel prozessModel)
        {
            tb_main.Text = prozessModel.ToString();

        }

        public void ShowDevData(DataModel dataModel)
        {
            tb_main.Text = dataModel.GetDataString();
        }

        public void ShowError(string errorString)
        {
            tb_main.Text += errorString;
        }

        public void ShowSaved(string savedString)
        {
            tb_main.Text = tb_main.Text + '\n' + '\n' + savedString + " wurde efolgreich in der Datenbank" +
                " gesichert!";
        }


        private void tab_showData_Click(object sender, EventArgs e)
        {
            

        }


        /// Enable components
        public void EnableSaveBtn(bool enable)
        {
            btn_save.Enabled = enable;
        }

        public void EnableClearBtn(bool enable)
        {
            btn_clear.Enabled = enable;
        }

        public void EnableRemoveBtn(bool enable)
        {
            btn_remove.Enabled = enable;
        }

        public void EnableReadBtn(bool enable)
        {
            btn_readPublic.Enabled = enable;

        }

        public void EnableReadPublicBtn(bool enable)
        {
            btn_readPublic.Enabled = enable;
        }

        public void EnableEditProzessBtn(bool enable)
        {
            btn_editProzess.Enabled = enable;
        }



        //Textbox
        public void SetTbCurrentProzess(int prozessId, string rkNr, string capturer, string date)
        {
            tb_currentProzess.Text = "Vorgang: " + prozessId + '\r' + '\n' +
                "RK_Nr.: " + rkNr + '\r' + '\n' +
                 "Erfasser: " + capturer + '\r' + '\n' + 
                "Datum: " + date;
        }

        public void SetTbCurrentDb(String path)
        {
            tb_CurrentDb.Text = path;
        }


        //Bindingsource
        public void SetSaveSelectBindingSource(BindingSource binding)
        {
            lb_DevItems.DataSource = binding;

        }





        #endregion




        #region Tab "Daten Anzeige"

        private void tb_searchFor_TextChanged(object sender, EventArgs e)
        {
            searchTxt = tb_searchFor.Text;
        }

        //Methodes



        //Enable Components

        public void EnableBtnSearchDevData(bool enable)
        {
            btn_searchDevData.Enabled = enable;
        }



        //Textbox

        public void SetTextTbShowData(string text)
        {
            tb_showData.Text = text;
        }

        private void tb_showData_Clicked(object sender, EventArgs e)
        {
            int position = tb_showData.SelectionStart;
            int line = tb_showData.GetLineFromCharIndex(position);
            int end = tb_showData.Text.IndexOf(Environment.NewLine, position);
            int start = tb_showData.GetFirstCharIndexFromLine(line);

            if (end >= 0)
            {
                tb_showData.Select(start, end - start);
            }

            Debug.WriteLine("Line: " + line);
            Debug.WriteLine("NewLine: " + end);


        }



        //Bindingsource

        public void SetDevDataDesignationsBindingSource(BindingSource binding)
        {
            cb_devDataFilter.DataSource = binding;

        }

        public void SetComparisonSymbolsBindingSource(BindingSource binding)
        {
            cb_comparisonSymbols.DataSource = binding;
        }


        // Events

        private void cb_devDataFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchDevDataDesignation = cb_devDataFilter.SelectedItem.ToString();

        }


        private void cb_comparisonSymbols_SelectedIndexChanged(object sender, EventArgs e)
        {
            comparisonSymbol = cb_comparisonSymbols.SelectedItem.ToString();

        }




        #endregion

    }
}
