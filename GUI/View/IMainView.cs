﻿using DB_Tool_FW.DB_and_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Tool_FW.GUI.View
{
    public interface IView
    {
        //Fields
        List<string> ReadCollection { get; set; }
        List<string> DataCollection { get; set; }
        string SelectedData { get; set; }



        //Eventhandler
        event EventHandler OnReadPublicClicked;
        event EventHandler OnSaveClicked;
        event EventHandler OnAdditionalDataClicked;
        event EventHandler OnRecordReturnes;
        event EventHandler OnClearClicked;
        event EventHandler OnNewProzessClicked;
        event EventHandler OnRemoveBtnClicked;

        void EnableSaveBtn(bool enable);
        void SetDevDataDesignationsBindingSource(BindingSource binding);
        void SetSaveSelectBindingSource(BindingSource binding);
        void SetTbCurrentProzess(int prozessId, string rkNr, string capturer, string date);
        void ShowError(string errorString);
        void ShowProzess(ProzessModel prozessModel);
    }





}




