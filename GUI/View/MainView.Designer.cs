﻿namespace DB_Tool_FW.GUI.View
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the ProzessPropertys of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab_showData = new System.Windows.Forms.TabPage();
            this.cb_comparisonSymbols = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btn_searchDevData = new System.Windows.Forms.Button();
            this.tb_DevDataSearchValue = new System.Windows.Forms.TextBox();
            this.cb_devDataFilter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_searchFor = new System.Windows.Forms.TextBox();
            this.btn_prozessSearch = new System.Windows.Forms.Button();
            this.tb_showData = new System.Windows.Forms.TextBox();
            this.tab_addData = new System.Windows.Forms.TabPage();
            this.lb_DevItems = new System.Windows.Forms.ListBox();
            this.btn_editProzess = new System.Windows.Forms.Button();
            this.tb_currentProzess = new System.Windows.Forms.TextBox();
            this.tb_main = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_newProzess = new System.Windows.Forms.Button();
            this.btn_readPublic = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tab_database = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_CurrentDb = new System.Windows.Forms.TextBox();
            this.btn_choseDb = new System.Windows.Forms.Button();
            this.btn_createDB = new System.Windows.Forms.Button();
            this.tab_showData.SuspendLayout();
            this.tab_addData.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tab_database.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_showData
            // 
            this.tab_showData.Controls.Add(this.cb_comparisonSymbols);
            this.tab_showData.Controls.Add(this.textBox1);
            this.tab_showData.Controls.Add(this.textBox3);
            this.tab_showData.Controls.Add(this.btn_searchDevData);
            this.tab_showData.Controls.Add(this.tb_DevDataSearchValue);
            this.tab_showData.Controls.Add(this.cb_devDataFilter);
            this.tab_showData.Controls.Add(this.label3);
            this.tab_showData.Controls.Add(this.label2);
            this.tab_showData.Controls.Add(this.tb_searchFor);
            this.tab_showData.Controls.Add(this.btn_prozessSearch);
            this.tab_showData.Controls.Add(this.tb_showData);
            this.tab_showData.Location = new System.Drawing.Point(4, 29);
            this.tab_showData.Name = "tab_showData";
            this.tab_showData.Padding = new System.Windows.Forms.Padding(3);
            this.tab_showData.Size = new System.Drawing.Size(1254, 695);
            this.tab_showData.TabIndex = 1;
            this.tab_showData.Text = "Daten Anzeige";
            this.tab_showData.UseVisualStyleBackColor = true;
            this.tab_showData.Click += new System.EventHandler(this.tab_showData_Click);
            // 
            // cb_comparisonSymbols
            // 
            this.cb_comparisonSymbols.FormattingEnabled = true;
            this.cb_comparisonSymbols.Location = new System.Drawing.Point(30, 398);
            this.cb_comparisonSymbols.Name = "cb_comparisonSymbols";
            this.cb_comparisonSymbols.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cb_comparisonSymbols.Size = new System.Drawing.Size(121, 28);
            this.cb_comparisonSymbols.TabIndex = 13;
            this.cb_comparisonSymbols.SelectedIndexChanged += new System.EventHandler(this.cb_comparisonSymbols_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(24, 298);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(238, 36);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "Suchen nach ...\r\n";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(30, 463);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(422, 53);
            this.textBox3.TabIndex = 11;
            this.textBox3.Text = "Wildcards:\r\n\'%\' Bsp: %spiel\r\n\'_\' Bsp: _eispie_\r\n";
            // 
            // btn_searchDevData
            // 
            this.btn_searchDevData.Location = new System.Drawing.Point(297, 298);
            this.btn_searchDevData.Name = "btn_searchDevData";
            this.btn_searchDevData.Size = new System.Drawing.Size(142, 70);
            this.btn_searchDevData.TabIndex = 10;
            this.btn_searchDevData.Text = "Suchen";
            this.btn_searchDevData.UseVisualStyleBackColor = true;
            // 
            // tb_DevDataSearchValue
            // 
            this.tb_DevDataSearchValue.Location = new System.Drawing.Point(175, 398);
            this.tb_DevDataSearchValue.Name = "tb_DevDataSearchValue";
            this.tb_DevDataSearchValue.Size = new System.Drawing.Size(277, 26);
            this.tb_DevDataSearchValue.TabIndex = 9;
            // 
            // cb_devDataFilter
            // 
            this.cb_devDataFilter.FormattingEnabled = true;
            this.cb_devDataFilter.Location = new System.Drawing.Point(24, 340);
            this.cb_devDataFilter.Name = "cb_devDataFilter";
            this.cb_devDataFilter.Size = new System.Drawing.Size(237, 28);
            this.cb_devDataFilter.TabIndex = 8;
            this.cb_devDataFilter.Text = "Suchen nach";
            this.cb_devDataFilter.SelectedIndexChanged += new System.EventHandler(this.cb_devDataFilter_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.LightGray;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(428, 50);
            this.label3.TabIndex = 7;
            this.label3.Text = "Device";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.LightGray;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(428, 50);
            this.label2.TabIndex = 6;
            this.label2.Text = "Prozess";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tb_searchFor
            // 
            this.tb_searchFor.Location = new System.Drawing.Point(30, 93);
            this.tb_searchFor.Name = "tb_searchFor";
            this.tb_searchFor.Size = new System.Drawing.Size(237, 26);
            this.tb_searchFor.TabIndex = 5;
            this.tb_searchFor.TextChanged += new System.EventHandler(this.tb_searchFor_TextChanged);
            // 
            // btn_prozessSearch
            // 
            this.btn_prozessSearch.Location = new System.Drawing.Point(297, 93);
            this.btn_prozessSearch.Name = "btn_prozessSearch";
            this.btn_prozessSearch.Size = new System.Drawing.Size(142, 85);
            this.btn_prozessSearch.TabIndex = 4;
            this.btn_prozessSearch.Text = "Suchen";
            this.btn_prozessSearch.UseVisualStyleBackColor = true;
            // 
            // tb_showData
            // 
            this.tb_showData.AcceptsReturn = true;
            this.tb_showData.AcceptsTab = true;
            this.tb_showData.HideSelection = false;
            this.tb_showData.Location = new System.Drawing.Point(590, 30);
            this.tb_showData.MaxLength = 1000000000;
            this.tb_showData.Multiline = true;
            this.tb_showData.Name = "tb_showData";
            this.tb_showData.ReadOnly = true;
            this.tb_showData.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb_showData.Size = new System.Drawing.Size(609, 627);
            this.tb_showData.TabIndex = 3;
            this.tb_showData.WordWrap = false;
            this.tb_showData.Click += new System.EventHandler(this.tb_showData_Clicked);
            // 
            // tab_addData
            // 
            this.tab_addData.AutoScroll = true;
            this.tab_addData.Controls.Add(this.lb_DevItems);
            this.tab_addData.Controls.Add(this.btn_editProzess);
            this.tab_addData.Controls.Add(this.tb_currentProzess);
            this.tab_addData.Controls.Add(this.tb_main);
            this.tab_addData.Controls.Add(this.label1);
            this.tab_addData.Controls.Add(this.btn_clear);
            this.tab_addData.Controls.Add(this.btn_remove);
            this.tab_addData.Controls.Add(this.btn_save);
            this.tab_addData.Controls.Add(this.btn_newProzess);
            this.tab_addData.Controls.Add(this.btn_readPublic);
            this.tab_addData.Location = new System.Drawing.Point(4, 29);
            this.tab_addData.Name = "tab_addData";
            this.tab_addData.Padding = new System.Windows.Forms.Padding(3);
            this.tab_addData.Size = new System.Drawing.Size(1254, 695);
            this.tab_addData.TabIndex = 0;
            this.tab_addData.Text = "Daten Eingabe";
            this.tab_addData.UseVisualStyleBackColor = true;
            // 
            // lb_DevItems
            // 
            this.lb_DevItems.FormattingEnabled = true;
            this.lb_DevItems.ItemHeight = 20;
            this.lb_DevItems.Location = new System.Drawing.Point(38, 131);
            this.lb_DevItems.Name = "lb_DevItems";
            this.lb_DevItems.Size = new System.Drawing.Size(157, 124);
            this.lb_DevItems.TabIndex = 16;
            // 
            // btn_editProzess
            // 
            this.btn_editProzess.Enabled = false;
            this.btn_editProzess.Location = new System.Drawing.Point(240, 336);
            this.btn_editProzess.Name = "btn_editProzess";
            this.btn_editProzess.Size = new System.Drawing.Size(165, 52);
            this.btn_editProzess.TabIndex = 13;
            this.btn_editProzess.Text = "Vorgang bearbeiten";
            this.btn_editProzess.UseVisualStyleBackColor = true;
            // 
            // tb_currentProzess
            // 
            this.tb_currentProzess.Location = new System.Drawing.Point(240, 229);
            this.tb_currentProzess.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_currentProzess.Multiline = true;
            this.tb_currentProzess.Name = "tb_currentProzess";
            this.tb_currentProzess.ReadOnly = true;
            this.tb_currentProzess.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.tb_currentProzess.Size = new System.Drawing.Size(163, 99);
            this.tb_currentProzess.TabIndex = 12;
            this.tb_currentProzess.WordWrap = false;
            // 
            // tb_main
            // 
            this.tb_main.AcceptsReturn = true;
            this.tb_main.Location = new System.Drawing.Point(567, 29);
            this.tb_main.Multiline = true;
            this.tb_main.Name = "tb_main";
            this.tb_main.ReadOnly = true;
            this.tb_main.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_main.Size = new System.Drawing.Size(609, 627);
            this.tb_main.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(278, 211);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 11;
            this.label1.Text = "Aktueller Vorgang";
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(38, 583);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(369, 52);
            this.btn_clear.TabIndex = 10;
            this.btn_clear.Text = "Clear Display";
            this.btn_clear.UseVisualStyleBackColor = true;
            // 
            // btn_remove
            // 
            this.btn_remove.Enabled = false;
            this.btn_remove.Location = new System.Drawing.Point(36, 276);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(165, 52);
            this.btn_remove.TabIndex = 9;
            this.btn_remove.Text = "Letzte Entfernen";
            this.btn_remove.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Enabled = false;
            this.btn_save.Location = new System.Drawing.Point(240, 394);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(165, 52);
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "Speichern";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_newProzess
            // 
            this.btn_newProzess.Location = new System.Drawing.Point(36, 29);
            this.btn_newProzess.Name = "btn_newProzess";
            this.btn_newProzess.Size = new System.Drawing.Size(492, 52);
            this.btn_newProzess.TabIndex = 2;
            this.btn_newProzess.Text = "Neuer Vorgang";
            this.btn_newProzess.UseVisualStyleBackColor = true;
            // 
            // btn_readPublic
            // 
            this.btn_readPublic.Location = new System.Drawing.Point(266, 121);
            this.btn_readPublic.Name = "btn_readPublic";
            this.btn_readPublic.Size = new System.Drawing.Size(128, 48);
            this.btn_readPublic.TabIndex = 0;
            this.btn_readPublic.Text = "Read Public";
            this.btn_readPublic.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tab_addData);
            this.tabControl.Controls.Add(this.tab_showData);
            this.tabControl.Controls.Add(this.tab_database);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1262, 728);
            this.tabControl.TabIndex = 0;
            // 
            // tab_database
            // 
            this.tab_database.Controls.Add(this.label4);
            this.tab_database.Controls.Add(this.tb_CurrentDb);
            this.tab_database.Controls.Add(this.btn_choseDb);
            this.tab_database.Controls.Add(this.btn_createDB);
            this.tab_database.Location = new System.Drawing.Point(4, 29);
            this.tab_database.Name = "tab_database";
            this.tab_database.Padding = new System.Windows.Forms.Padding(3);
            this.tab_database.Size = new System.Drawing.Size(1254, 695);
            this.tab_database.TabIndex = 2;
            this.tab_database.Text = "Datenbank";
            this.tab_database.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(178, 311);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Aktuelle DB";
            // 
            // tb_CurrentDb
            // 
            this.tb_CurrentDb.Location = new System.Drawing.Point(105, 334);
            this.tb_CurrentDb.Name = "tb_CurrentDb";
            this.tb_CurrentDb.Size = new System.Drawing.Size(411, 26);
            this.tb_CurrentDb.TabIndex = 2;
            this.tb_CurrentDb.Text = "Keine Datenbank ausgewählt";
            // 
            // btn_choseDb
            // 
            this.btn_choseDb.Location = new System.Drawing.Point(182, 118);
            this.btn_choseDb.Name = "btn_choseDb";
            this.btn_choseDb.Size = new System.Drawing.Size(113, 83);
            this.btn_choseDb.TabIndex = 1;
            this.btn_choseDb.Text = "Datenbank Auswählen";
            this.btn_choseDb.UseVisualStyleBackColor = true;
            // 
            // btn_createDB
            // 
            this.btn_createDB.Location = new System.Drawing.Point(48, 118);
            this.btn_createDB.Name = "btn_createDB";
            this.btn_createDB.Size = new System.Drawing.Size(119, 83);
            this.btn_createDB.TabIndex = 0;
            this.btn_createDB.Text = "Neue Datenbank erstellen";
            this.btn_createDB.UseVisualStyleBackColor = true;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1262, 728);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(1274, 758);
            this.Name = "MainView";
            this.Text = "DBTool";
            this.Load += new System.EventHandler(this.MainView_Load);
            this.tab_showData.ResumeLayout(false);
            this.tab_showData.PerformLayout();
            this.tab_addData.ResumeLayout(false);
            this.tab_addData.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tab_database.ResumeLayout(false);
            this.tab_database.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tab_showData;
        private System.Windows.Forms.TabPage tab_addData;
        private System.Windows.Forms.TextBox tb_currentProzess;
        private System.Windows.Forms.TextBox tb_main;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Button btn_remove;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_newProzess;
        private System.Windows.Forms.Button btn_readPublic;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TextBox tb_showData;
        private System.Windows.Forms.TabPage tab_database;
        private System.Windows.Forms.TextBox tb_searchFor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_searchDevData;
        private System.Windows.Forms.TextBox tb_DevDataSearchValue;
        private System.Windows.Forms.ComboBox cb_devDataFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_prozessSearch;
        private System.Windows.Forms.Button btn_editProzess;
        private System.Windows.Forms.ListBox lb_DevItems;
        private System.Windows.Forms.Button btn_choseDb;
        private System.Windows.Forms.Button btn_createDB;
        private System.Windows.Forms.TextBox tb_CurrentDb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cb_comparisonSymbols;
    }
}